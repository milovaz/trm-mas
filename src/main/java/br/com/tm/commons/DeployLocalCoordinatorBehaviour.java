package br.com.tm.commons;

import br.com.tm.cloudmanager.localcoordinator.LocalCoordinatorAgent;
import jade.core.behaviours.OneShotBehaviour;

public class DeployLocalCoordinatorBehaviour extends OneShotBehaviour {

	private static final long serialVersionUID = 1L;

	@Override
	public void action() {
		if(getAgent() instanceof LocalCoordinatorAgent) {
			LocalCoordinatorAgent lc = (LocalCoordinatorAgent) getAgent();
			lc.restartLocalCoordinator();
		}
	}

}
