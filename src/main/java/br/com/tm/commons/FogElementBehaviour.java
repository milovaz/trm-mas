package br.com.tm.commons;

import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class FogElementBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;

	private static final String CFP_LOCAL_COORDINATORS = "cfp-local-coordinators";
	
	private int proposal = 1000;
	
	MessageTemplate mtGetRatings = MessageTemplate.and(
		MessageTemplate.MatchConversationId(CFP_LOCAL_COORDINATORS),
		MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET)
	);
	
	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mtGetRatings);
		if(msg != null) {
			if(msg.getPerformative() == ACLMessage.CFP) {
				ACLMessage reply = msg.createReply();
				reply.setContent(String.valueOf(proposal));
				reply.setPerformative(ACLMessage.PROPOSE);
				myAgent.send(reply);
				
				System.out.println("Sending proposal for Local Coordinator");
			} else if(msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) {
				ACLMessage reply = msg.createReply();
				reply.setContent("OK");
				reply.setPerformative(ACLMessage.INFORM);
				
				myAgent.addBehaviour(new DeployLocalCoordinatorBehaviour());
				
				myAgent.send(reply);
				
				System.out.println("Received acceptance for local coordinator deploy");
			}
		} else {
			block();
		}
	}

}
