package br.com.tm.node;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;

import br.com.tm.util.ServerInfo;
import br.com.tm.util.SupernodeSearchResponse;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class RequestServiceBehaviour extends Behaviour {

	private static final long serialVersionUID = 1L;

	private static final String SUPERNODES_SEARCH_CONVERSATION_ID = "supernodes-search";
	
	MessageTemplate mt = MessageTemplate.and(
			MessageTemplate.MatchConversationId(SUPERNODES_SEARCH_CONVERSATION_ID),
			MessageTemplate.and(
				MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
				MessageTemplate.or(
					MessageTemplate.MatchPerformative(ACLMessage.INFORM),
					MessageTemplate.MatchPerformative(ACLMessage.FAILURE)
				)
			)
	);
	
	private static final int REQUEST_SUPERNODES_LIST = 0;
	private static final int WAIT_FOR_SUPERNODES_LIST_RESPONSE = 1;
	
	private List<ServerInfo> supernodesList;
	private List<AID> supernodesCandidatesReferralList;
	private int step = REQUEST_SUPERNODES_LIST;
	
	private boolean forceSearchOnCloud;
	
	public RequestServiceBehaviour() {
		this.supernodesList = new ArrayList<>();
		this.supernodesCandidatesReferralList = new ArrayList<>();
		this.forceSearchOnCloud = false;
	}
	
	@Override
	public void action() {
		switch (step) {
			case REQUEST_SUPERNODES_LIST:
				ACLMessage requestServiceMessage = new ACLMessage(ACLMessage.REQUEST);
				requestServiceMessage.setConversationId(SUPERNODES_SEARCH_CONVERSATION_ID);
				requestServiceMessage.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
				
				if(getAgent().localCoordinator != null && !forceSearchOnCloud) {
					requestServiceMessage.addReceiver(getAgent().localCoordinator);
					System.out.println(myAgent.getName() +  " - Sending request service message to " + getAgent().localCoordinator.getLocalName());
				} else if(getAgent().cloudManagers != null) {
					for(AID cloudManager : getAgent().cloudManagers) {
						requestServiceMessage.addReceiver(cloudManager);
						System.out.println(myAgent.getName() +  " - Sending request service message to " + cloudManager.getLocalName());
					}
				}
				
				String content = getAgent().getLatitude() + ";" + getAgent().getLongitude();
				requestServiceMessage.setContent(content);
				requestServiceMessage.setReplyWith("sn-search-"+System.currentTimeMillis());
				
				getAgent().send(requestServiceMessage);
				getAgent().nodeStatus = NodeStatus.SEARCHING_SERVER;
				
				getAgent().traceReport.add("REQUESTED_SUPERNODES_LIST;" + Clock.systemUTC().millis());
				
				getAgent().lcStats.setCountInterations(getAgent().lcStats.getCountInterations() + 1);
				
				step = WAIT_FOR_SUPERNODES_LIST_RESPONSE;
			break;
			
			case WAIT_FOR_SUPERNODES_LIST_RESPONSE:
				try {
					ACLMessage reply = myAgent.receive(mt);
					if(reply != null) {
						if(reply.getPerformative() == ACLMessage.INFORM) {
							SupernodeSearchResponse supernodeSearchResponse = reply.getContentObject() != null ? (SupernodeSearchResponse)reply.getContentObject() : null;
							
							if(supernodeSearchResponse != null) { 
								this.supernodesList = supernodeSearchResponse.supernodes;
								if(supernodeSearchResponse.referrals != null) {
									this.supernodesCandidatesReferralList = supernodeSearchResponse.referrals; 
								}
							}
							 
							System.out.println("Node - " + myAgent.getLocalName() + " - received response from cloud");
							
							if(this.supernodesList == null || this.supernodesList.isEmpty()) {
								System.out.println("Node  - " + myAgent.getLocalName() + " - received a empty supernode list");
							}
							
							getAgent().supernodesCandidatesList = this.supernodesList;
							getAgent().supernodesCandidatesReferralList = this.supernodesCandidatesReferralList;
							
							this.supernodesCandidatesReferralList.forEach(s -> System.out.println(s.getName()));
							
							getAgent().traceReport.add("RECEIVED_SUPERNODES_LIST;" + Clock.systemUTC().millis());
							
							getAgent().addBehaviour(new SelectServerBehaviour(this.supernodesList));
							
							step = 3;
						} else if(reply.getPerformative() == ACLMessage.FAILURE){
							if(getAgent().localCoordinator != null && !forceSearchOnCloud) {
								getAgent().lcStats.setCountDownTimes(getAgent().lcStats.getCountDownTimes() + 1);
								System.out.println("Local coordinator failure");
							}
							
							forceSearchOnCloud = true;
							
							step = REQUEST_SUPERNODES_LIST;
						}
					} else {
						block();
					}
				} catch (UnreadableException e) {
					e.printStackTrace();
				}
			break;
		}
	}

	@Override
	public boolean done() {
		return this.step == 3;
	}
	
	public NodeAgent getAgent() {
		return (NodeAgent) myAgent;
	}

}
