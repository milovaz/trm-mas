package br.com.tm.node;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import br.com.tm.client.UDPClient;
import br.com.tm.commons.FogElementBehaviour;
import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.report.Report;
import br.com.tm.report.ReportFactory;
import br.com.tm.stream.MessageStats;
import br.com.tm.util.Configuration;
import br.com.tm.util.FogServiceModel;
import br.com.tm.util.Geolocation;
import br.com.tm.util.LCStats;
import br.com.tm.util.ServerInfo;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.tools.introspector.gui.MyDialog;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

public class NodeAgent extends Agent {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Parameters for simulation and tests 
	 */
	public static final int SECOND = 1000; 
	public static final int MINUTE = SECOND; //Adjusting - means that 1 MINUTE == 1 SECOND
	public static final int HOUR = 60 * MINUTE;
	public static final int DAY = 24 * HOUR; 
	
	private static final int SUBCYCLE = HOUR;
	private static final int CYCLE = DAY;
	
	protected long startTime;
	protected long endTime;
	protected int duration;
	protected long tick;
	protected int trm;
	/************************************************************/
	
	protected static final int CLOUD = 0;
	protected static final int SUPERNODE = 1;
	
	/**
	 * Knowlegde base
	 */
	protected final double lossPctThreshold = 0.05; 
	protected final int ratingHistorySize = 7;
	protected int latencyRequirement = 25;
	protected final double ratingSatisfactionThreshold = 0.5;
	protected final double ratingPctSatisfactionForChange = 0.6;
	protected int supernodeCheckPoint = 4;
	protected double localCoodinatorDownThreshold = 0.1;
	protected int localCoordinatorIterationRatingThreshold = 7;
	protected double muParameter = 21.0/5.0;
	/***********************************************/
	
	protected AID[] cloudManagers;
	
	protected ServerInfo[] cloudManagersServerInfo;
	
	protected UDPClient udpClient;
	
	protected Thread udpClientThread;
	
	protected AID server;
	
	protected AID localCoordinator;
	
	protected int serverType;
	
	protected ServerInfo serverInfo;
	
	protected Map<String, Date> sharedRatingsMapControl;
	
	protected Map<String, List<MessageStats>> serviceStats;
	
	protected HashMap<String, List<Rating>> localRatings;
	
	protected HashMap<String, List<Rating>> ratingsTransmissionCache;
	
	protected List<ServerInfo> supernodesCandidatesList;
	
	protected List<AID> supernodesCandidatesReferralList;
	
	protected LCStats lcStats = new LCStats();
	
	protected Report report;
	
	protected Report traceReport;
	
	protected Report localCoordinatorReport;
	
	protected NodeStatus nodeStatus;
	protected FogServiceModel fogServiceModel = null;
	
	protected BlockingQueue<Object> clientQueue;
	
	protected Double latitude;
	protected Double longitude;
	
	protected String logCache = "";
	
	protected int interationCount = 1;
	
	protected NodeTRMApproach nodeTRMApproach = null;
	protected int nodeTRMApproachValue = -1;
	protected int iterationTRMThreshold;
	
	protected Map<String, Double> cloudTransmissionDelay;
	
	private Thread clockThread;
	
	private Boolean canBeLocalCoordinator = null;
	
	protected int setupNewAgent;
	
	@Override
	protected void setup() {
		super.setup();
		
		report = ReportFactory.getReport(this);
		traceReport = ReportFactory.getTraceReport(this);
		localCoordinatorReport = ReportFactory.getReport("lc-report-" + getLocalName());
		
		this.server = null;
		this.localRatings = new HashMap<>();
		this.serviceStats = new HashMap<>();
		this.ratingsTransmissionCache = new HashMap<>();
		this.sharedRatingsMapControl = new HashMap<>();
		this.nodeStatus = NodeStatus.INACTIVE;
		this.clientQueue = new LinkedBlockingQueue<Object>(); 
		this.cloudTransmissionDelay = new HashMap<>();
		
		getParametersFromArguments("startTime", "endTime", "tick", "latitude", "longitude", "trm", "latencyRequirement", "nodeTRMApproachValue", "iterationTRMThreshold", "canBeLocalCoordinator", "setupNewAgent");
		
		if(latitude == null || longitude == null) {
			setUpLatLong();
		}
		
		if(trm < 0) {
			trm = Configuration.RANDOM;
		}
		
		if(nodeTRMApproachValue >= 0) {
			nodeTRMApproach = NodeTRMApproach.parse(nodeTRMApproachValue);
		}
		
		if(iterationTRMThreshold == 0) {
			iterationTRMThreshold = 7;
		}
		
		if(canBeLocalCoordinator == null) {
			canBeLocalCoordinator = false;
		}
		
		this.tick = this.tick > 0 ? this.tick : 60000;
		
		findCloudManagers();
		
		addBehaviour(new NodeStartupBehaviour());
		addBehaviour(new NodeCommunicationInterfaceBehaviour(clientQueue));
		if(canBeLocalCoordinator) {
			addBehaviour(new FogElementBehaviour());
		}
	}
	
	protected void findCloudManagers() {
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("cloud-manager");
		template.addServices(sd);
		
		try {
			DFAgentDescription[] result = DFService.search(this, template);
			if(result.length > 0) {
				cloudManagers = new AID[result.length];
				for (int i = 0; i < result.length; ++i) {
					cloudManagers[i] = result[i].getName();
				}
			} else {
				
			}
		} catch (FIPAException e) {
			e.printStackTrace();
		}
	}
	
	protected double utilityFunction(double x) {
		//return 1 - Math.exp(-muParameter * Math.log(x*(7.0/4.0)));
		//return 1 - 32 * Math.exp(-7 * x);
		return 1 - Math.exp(-muParameter * x);
	}
	
	protected double outcomeProbabilityForSupernode(Double[] trmValue) {
		return trmValue[0] * trmValue[1];
	}
	
	protected double outcomeProbabilityForCloud(String cloudManager) {
		//return  1 - Math.exp(-muParameter * Math.log(0.7 * (7.0/4.0)));
		//return 1 - 32 * Math.exp(-7 * 0.7);
		return 0.7;
	}
	
	protected Map<String, Double> calculateUtilityForSupernodes(Map<String, Double[]> trmForSupernodes) {
		Map<String, Double> utilities = new HashMap<>();
		int end = (int)(this.lossPctThreshold * 100);
		
		// Generate lottery
		for(Entry<String, Double[]> entry : trmForSupernodes.entrySet()) {
			double lotteryValue = 0;
			
			System.out.println(entry.getKey() + " - " + entry.getValue()[0] + "/" + entry.getValue()[1]);
			
			for(double x = 0.8; x < 1; x += 0.01) {
				lotteryValue += outcomeProbabilityForSupernode(entry.getValue()) * utilityFunction(x);
			}
			
			if(Double.isNaN(lotteryValue)) {
				lotteryValue = 0.0;
			}
			
			utilities.put(entry.getKey(), lotteryValue);
		}
		
		return utilities;
	}
	
	protected Map<String, Double> calculateUtilityForClouds() {
		Map<String, Double> utilities = new HashMap<>();
		double lotteryValue = 0;
		for(AID cloudManager : this.cloudManagers) {
			for(double x = 0.6; x < 1; x += 0.01) {
				lotteryValue += outcomeProbabilityForCloud(cloudManager.getName()) * utilityFunction(x);
				utilities.put(cloudManager.getName(), lotteryValue);
			}
		}
		
		return utilities;
	}

	public double getLossPctThreshold() {
		return lossPctThreshold;
	}

	public int getRatingHistorySize() {
		return ratingHistorySize;
	}

	public int getLatencyRequirement() {
		return latencyRequirement;
	}

	protected void getParametersFromArguments(String ... paramatersName) {
		if(getArguments() != null && getArguments().length > 0) {
			for(String parameter : paramatersName) {
				int i = 0;
				while(i < getArguments().length) {
					Object a = getArguments()[i]; 
					if(a instanceof String) {
						if(((String) a).equalsIgnoreCase(parameter)) {
							try {
								if(i + 1 < getArguments().length) {
									if(Arrays.stream(this.getClass().getDeclaredFields()).filter(f -> f.getName().equals(parameter)).count() > 0) {
										this.getClass().getDeclaredField(parameter).set(this, dynamicTypeSetting(this.getClass().getDeclaredField(parameter).getType(), getArguments()[i + 1].toString()));
									} else {
										System.out.println(this.getClass().getSuperclass().getName());
										this.getClass().getSuperclass().getDeclaredField(parameter).set(this, dynamicTypeSetting(this.getClass().getDeclaredField(parameter).getType(), getArguments()[i + 1].toString()));
									}
									
									i+=2;
									break;
								}
							} catch (NumberFormatException e) {
								throw new RuntimeException("Invalid arguments for capacity parameter");
							} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
								e.printStackTrace();
								throw new RuntimeException("Invalid arguments for capacity parameter");
							}
						}
					}
					i++;
				}
			}
		}
	}
	
	protected Object dynamicTypeSetting(Class fieldType, String value) {
		if(fieldType.getName().equals(Integer.class.getName()) || fieldType.getName().equals("int")) {
			return Integer.valueOf(value);
		} else if(fieldType.getName().equals(Long.class.getName()) || fieldType.getName().equals("long")) {
			return Long.valueOf(value);
		} else if(fieldType.getName().equals(Double.class.getName()) || fieldType.getName().equals("double")) {
			return Double.valueOf(value);
		} else if(fieldType.getName().equals(Boolean.class.getName()) || fieldType.getName().equals("boolean")) {
			return Boolean.valueOf(value);
		}
		
		return null;
	}
	
	protected void collectData() {
		if(this.serviceStats != null && !this.serviceStats.isEmpty()) {
			HashMap<String, List<Rating>> localRatingsAux = this.localRatings == null ? new HashMap<>() : this.localRatings;
			for(Entry<String, List<MessageStats>> entry : this.serviceStats.entrySet()) {
				MessageStats mStats = new MessageStats(0, 0, 0, 0);
				entry.getValue().forEach(m -> {
					mStats.countMessages += m.countMessages;
					mStats.countLosses += m.countLosses;
					mStats.latency = mStats.latency == 0 ? m.latency : (mStats.latency + m.latency) / 2;
				});
				mStats.server = this.server.getName();
				
				Rating rating = createRating(mStats);
				
				if(!localRatingsAux.containsKey(entry.getKey())) {
					localRatingsAux.put(entry.getKey(), new ArrayList());
				} else {
					int size = 0;
					int largest = -1;
					String largestEntry = null;
					for(Entry<String, List<Rating>> lr : localRatingsAux.entrySet()) {
						size += lr.getValue().size();
						if(lr.getValue().size() > largest) {
							largestEntry = lr.getKey();
							largest = lr.getValue().size(); 
						}
					}
					
					if(size > ratingHistorySize && largest >= 0 && localRatingsAux != null && !localRatingsAux.isEmpty() && localRatingsAux.get(largest) != null && !localRatingsAux.get(largest).isEmpty()) {
						localRatingsAux.get(largest).remove(0);
					}
				}
				
				localRatingsAux.get(entry.getKey()).add(rating);
				
				if(!ratingsTransmissionCache.containsKey(entry.getKey())) {
					ratingsTransmissionCache.put(entry.getKey(), new ArrayList());
				}
				
				ratingsTransmissionCache.get(entry.getKey()).add(rating);
				logCache += this.getReport().addRatingData(rating, mStats) + "\n";
				
			}
			
			this.localRatings = localRatingsAux;
			this.serviceStats = new HashMap<>();
		}
		
		if(this.lcStats != null) {
			localCoordinatorReport.add(lcStats.getCountDownTimes() + ";" + lcStats.getCountInterations() + ";" + localCoodinatorDownThreshold);
		}
	}
	
	private Rating createRating(MessageStats messageStats) {
		double ratingValue = 1D - (double)((double)messageStats.countLosses / (double)messageStats.countMessages);
		boolean satisfied = (double)((double)messageStats.countLosses / (double)messageStats.countMessages) < this.lossPctThreshold;
		
		return new Rating(this.getName(), messageStats.server, ratingValue, ratingValue, interationCount, "delay", (double)((double)messageStats.countLosses / (double)messageStats.countMessages), this.latencyRequirement, new Date(), satisfied);
	}
	
	protected void setupFogClient(long executionDuration) {
		if(this.udpClient != null) {
			this.udpClient.setServer(this.server.getName());
			this.udpClient.setServerIp(this.serverInfo.getServerIp());
			this.udpClient.setServerPort(this.serverInfo.getPort());
			this.udpClient.setExecutionDuration(executionDuration);
		} else {
			this.udpClient = new UDPClient(this.getName(), this.server.getName(), this.serverInfo.getServerIp(), this.serverInfo.getPort(), this.latencyRequirement, executionDuration, this.clientQueue);
		}
	}
	
	protected void startFogClient() {
		this.udpClientThread = new Thread(this.udpClient);
		this.udpClientThread.start();
	}
	
	protected void stopFogClient() {
		System.out.println("Stoping fog client");
		
		// This is far away of been the best solution, but i will put here only for the simulation tests since
		// a real use case wouldn't require a communication from the Agent to the FogClient
		if(this.udpClient != null) {
			//this.udpClient.stop();
			this.udpClientThread.stop();
			this.udpClient = null;
		}
	}
	
	protected List<AID> getSupernodeReferrals() {
		if(supernodesCandidatesReferralList != null && !supernodesCandidatesReferralList.isEmpty()) {
			return supernodesCandidatesReferralList.size() > 5 ? supernodesCandidatesReferralList.subList(0, 5) : supernodesCandidatesReferralList; 
		}
		
		return null;
	}
	
	protected void createNewNodeAgent() {
		try {
			AgentController ac = getContainerController().createNewAgent(this.getLocalName() + "_1", 
													NodeAgent.class.getName(), 
													new Object[] {"trm",this.trm,"latencyRequirement",this.latencyRequirement,"latitude",this.latitude,"longitude",this.longitude,"iterationTRMThreshold",this.iterationTRMThreshold});
			ac.start();
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
	}
	
	public void setCloudManagers(AID[] cloudManagers) {
		this.cloudManagers = cloudManagers;
	}

	public AID[] getCloudManagers() {
		return cloudManagers;
	}

	public Report getReport() {
		return report;
	}

	public void setReport(Report report) {
		this.report = report;
	}

	public Report getTraceReport() {
		return traceReport;
	}

	public void setTraceReport(Report traceReport) {
		this.traceReport = traceReport;
	}

	private int getCurrentDay() {
		return GregorianCalendar.getInstance().get(Calendar.DAY_OF_MONTH);
	}
	
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	protected void setUpLatLong() {
		try {
			URL urlIpGeolocation = new URL("http://ip-api.com/csv");
			URLConnection urlc = urlIpGeolocation.openConnection();

			urlc.setDoOutput(true);
			urlc.setConnectTimeout(90000);
			urlc.setReadTimeout(90000);
			
	        //get result
	        BufferedReader br = new BufferedReader(new InputStreamReader(urlc
	            .getInputStream()));
	        String l = null;
	        while ((l=br.readLine())!=null) {
	            String [] responseParts = l.split(",");
	            this.latitude = Double.parseDouble(responseParts[7]);
	            this.longitude = Double.parseDouble(responseParts[8]);
	        }
	        br.close();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException | NullPointerException e) {
			// TODO: handle exception
			System.out.println("Location CSV malformed - can't find lat/long");
		}

	}
	
	public FogServiceModel getFogServiceModel() {
		return fogServiceModel;
	}

	public void setFogServiceModel(FogServiceModel fogServiceModel) {
		this.fogServiceModel = fogServiceModel;
	}

	public AID getLocalCoordinator() {
		return localCoordinator;
	}

	public void setLocalCoordinator(AID localCoordinator) {
		this.localCoordinator = localCoordinator;
	}

	protected void startClock() {
		//Clock ticker
		this.clockThread = new Thread(new Runnable() {
			@Override
			public void run() {
				Instant i = Instant.ofEpochMilli(new Date().getTime() + Configuration.DAY);
				while(true) {
					while(Clock.tickSeconds(ZoneId.systemDefault()).instant().compareTo(i) < 0) { 	
					
					}
					//interationCount++;
					i = Instant.ofEpochMilli(new Date().getTime() + Configuration.DAY);
				}
			}
		});
		this.clockThread.start();
	}
	
	protected void stopClock() {
		if(this.clockThread.isAlive()) {
			this.clockThread.stop();
		}
	}
	
	protected Geolocation getGeolocation() {
		return new Geolocation(latitude, longitude);
	}
	
	@Override
	public void doDelete() {
		report.close();
		traceReport.close();
		super.doDelete();
	}
	
	@Override
	public void doSuspend() {
		this.nodeStatus = NodeStatus.INACTIVE;
		
		super.doSuspend();
	}
}
