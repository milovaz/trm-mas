package br.com.tm.node.supernode;

import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.SerializationUtils;

import br.com.tm.node.NodeAgent;
import br.com.tm.util.FogServiceModel;
import br.com.tm.util.SupernodeRegistryEnvelop;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class SupernodeStartupBehaviour extends Behaviour {

	private static final long serialVersionUID = 1L;
	
	private static final String SIMULATION_PARAMAS_CONVERSATION_ID = "simulation-params-conversation-id";
	private static final String SUPERNODE_REGISTRATION_CONVERSATION_ID = "supernode-registration-conversation-id";
	
	MessageTemplate mtSuperNodeRegistryMessage = MessageTemplate.and(
			MessageTemplate.MatchConversationId(SUPERNODE_REGISTRATION_CONVERSATION_ID),
			MessageTemplate.and(
					MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE),
					MessageTemplate.or(
						MessageTemplate.MatchPerformative(ACLMessage.AGREE),
						MessageTemplate.MatchPerformative(ACLMessage.REFUSE)
					)
				)
	);
	
	MessageTemplate mtParamsMessage = MessageTemplate.and(
			MessageTemplate.MatchConversationId(SIMULATION_PARAMAS_CONVERSATION_ID),
			MessageTemplate.and(
					MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE),
					MessageTemplate.MatchPerformative(ACLMessage.INFORM)
				)
	);
	
	private static final int FIND_CLOUD_MANAGER = 0;
	private static final int REGISTER_AS_SUPERNODE_CLOUD = 1;
	private static final int WAITING_RESPONSE_FROM_CLOUD = 2;
	private static final int REGISTER_AS_SUPERNODE_TO_LOCAL_COORDINATOR = 3;
	private static final int WAITING_RESPONSE_FROM_LOCAL_COORDINATOR = 4;
	private static final int WAITING_FOR_PARAMETERS = 5;
	private static final int STOPING_SUPERNODE = 6;
	private static final int START_SERVER = 7;
	private static final int HOLDING_FOR_SIMULATION_INIT = 8;
	private static final int END = 9;
	
	private long simulationInitTime;
	private long start;
	private long duration;
	private int step = FIND_CLOUD_MANAGER;
	
	private int countWait = 0;
	
	private String replyWithId;
	
	@Override
	public void action() {
		switch (step) {
			case FIND_CLOUD_MANAGER: 
				DFAgentDescription template = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("cloud-manager");
				template.addServices(sd);
				
				try {
					DFAgentDescription[] result = DFService.search(getAgent(), template);
					if(result.length > 0) {
						AID [] cloudManagers = new AID[result.length];
						for (int i = 0; i < result.length; ++i) {
							cloudManagers[i] = result[i].getName();
						}
						getAgent().setCloudManagers(cloudManagers);
						step = REGISTER_AS_SUPERNODE_CLOUD;
					} else {
						block(1000);
					}
				} catch (FIPAException e) {
					e.printStackTrace();
				}
			break;
			
			case REGISTER_AS_SUPERNODE_CLOUD:
				ACLMessage message = new ACLMessage(ACLMessage.SUBSCRIBE);
				message.setConversationId(SUPERNODE_REGISTRATION_CONVERSATION_ID);
				message.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
				
				if(getAgent().getCloudManagers() != null) {
					for(AID nodeAID : getAgent().getCloudManagers()) {
						message.addReceiver(nodeAID);
					}
				}
				
				message.setContent(getAgent().capacity + ";" + getAgent().getLatitude() + ";" + getAgent().getLongitude() + ";" + getAgent().serverPort + ";" + getAgent().getIp());
				message.setReplyWith("supernode-reg-"+System.currentTimeMillis());
				
				myAgent.send(message);
				
				System.out.println("Sending connection request to fog");
				getAgent().getTraceReport().add("Sending connection request to fog");
				
				step = WAITING_RESPONSE_FROM_CLOUD;
			break;
			
			case WAITING_RESPONSE_FROM_CLOUD:
				ACLMessage msgNodeRegistry = myAgent.receive(mtSuperNodeRegistryMessage);
				if(msgNodeRegistry != null) {
					if(msgNodeRegistry.getPerformative() == ACLMessage.AGREE) {
						try {
							System.out.println("Received AGREE response for fog connection");
							getAgent().getTraceReport().add("Received AGREE response for fog connection");
							
							SupernodeRegistryEnvelop contentEnvelop = (SupernodeRegistryEnvelop) msgNodeRegistry.getContentObject();
							getAgent().failurePct = contentEnvelop.pctToFail;
							getAgent().setFogServiceModel(FogServiceModel.parse(contentEnvelop.fogServiceModelValue));
							
							System.out.println(contentEnvelop.toString());
							
							if((getAgent().getFogServiceModel() == FogServiceModel.FIXED || getAgent().getFogServiceModel() == FogServiceModel.PARTIALLY_OPEN) && 
								contentEnvelop.localCoordinator != null) {
								getAgent().setLocalCoordinator(contentEnvelop.localCoordinator);
							}
							
							if(getAgent().getFogServiceModel() != null) {
								if(getAgent().getFogServiceModel() == FogServiceModel.FIXED || 
								   getAgent().getFogServiceModel() == FogServiceModel.PARTIALLY_OPEN) {
									if(getAgent().getLocalCoordinator() != null) {
										step = REGISTER_AS_SUPERNODE_TO_LOCAL_COORDINATOR;
									} else {
										if(countWait == 0) {
											block(1000);
											countWait++;
										} else {
											step = END;
										}
									}
								} else {
									step = END;
								}
							} else {
								block(1000);
							}
						} catch (UnreadableException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
					} else {
						System.out.println("Received REFUSE response for fog connection");
						getAgent().getTraceReport().add("Received REFUSE response for fog connection");
						
						step = STOPING_SUPERNODE;
					}
				} else {
					block();
				}
			break;
			
			case REGISTER_AS_SUPERNODE_TO_LOCAL_COORDINATOR:
				replyWithId = "lc-supernode-reg-"+System.currentTimeMillis();
				ACLMessage messageToLocalCoodinator = new ACLMessage(ACLMessage.SUBSCRIBE);
				messageToLocalCoodinator.setConversationId(SUPERNODE_REGISTRATION_CONVERSATION_ID);
				messageToLocalCoodinator.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
				messageToLocalCoodinator.addReceiver(getAgent().getLocalCoordinator());
				messageToLocalCoodinator.setContent(getAgent().capacity + ";" + getAgent().getLatitude() + ";" + getAgent().getLongitude() + ";" + getAgent().serverPort + ";" + getAgent().getIp());
				messageToLocalCoodinator.setReplyWith(replyWithId);
				
				myAgent.send(messageToLocalCoodinator);
				
				System.out.println("Sending connection request to local coodinator");
				getAgent().getTraceReport().add("Sending connection request to local coordinator");
				
				step = WAITING_RESPONSE_FROM_LOCAL_COORDINATOR;
			break;
			
			case WAITING_RESPONSE_FROM_LOCAL_COORDINATOR:
				ACLMessage msgLCSupernodeRegistry = myAgent.receive(MessageTemplate.and(
						MessageTemplate.MatchInReplyTo(replyWithId),
						MessageTemplate.and(
								MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE),
								MessageTemplate.or(
									MessageTemplate.MatchPerformative(ACLMessage.AGREE),
									MessageTemplate.MatchPerformative(ACLMessage.REFUSE)
								)
							)
				));
				if(msgLCSupernodeRegistry != null) {
					if(msgLCSupernodeRegistry.getPerformative() == ACLMessage.AGREE) {
						System.out.println("Received AGREE response for local coordinator connection");
						getAgent().getTraceReport().add("Received AGREE response for local coordinator connection");
					} else {
						System.out.println("Received REFUSE response for fog connection");
						getAgent().getTraceReport().add("Received REFUSE response for fog connection");
						getAgent().registeredWithLocalCoordinator = false;
					}
					
					step = END;
				}
			break;
		
			case END:
				//getAgent().iterationDigest();
				//getAgent().startClock();
				step++;
			break;
		}		
	}

	@Override
	public boolean done() {
		return step > END;
	}
	
	@Override
	public SupernodeAgent getAgent() {
		return (SupernodeAgent) myAgent;
	}
}
