package br.com.tm.node.supernode;

import java.util.Date;

import br.com.tm.util.FogServiceModel;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class LocalFogServiceBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;
	
	private static final String ADD_TO_SUPPORTED_LIST = "add-to-supported-list";
	private static final String NODE_DISCONNECT_FOG_SERVER_CONVERSATION_ID = "node-disconnect-fog-server-conversation-id";
	private static final String COLLECT_SATISFACTION_DATA_CONVERSATION_ID = "collect-data-conversation-id";
	
	MessageTemplate mtSubscribeToLocalService = MessageTemplate.and(
			MessageTemplate.or(
					MessageTemplate.MatchConversationId(ADD_TO_SUPPORTED_LIST),
					MessageTemplate.MatchConversationId(NODE_DISCONNECT_FOG_SERVER_CONVERSATION_ID)
			),
			MessageTemplate.and(
					MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE),
					MessageTemplate.or(
							MessageTemplate.MatchPerformative(ACLMessage.SUBSCRIBE),
							MessageTemplate.MatchPerformative(ACLMessage.CANCEL)
					)
			)
			
	);

	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mtSubscribeToLocalService);
		if(msg != null) {
			ACLMessage reply = msg.createReply();
			reply.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
			switch (msg.getPerformative()) {
				case ACLMessage.SUBSCRIBE:
					if(((SupernodeAgent) myAgent).isAlive() && canSupport()) {
						reply.setPerformative(ACLMessage.AGREE);
						addToSupportedNodeList(msg.getSender());
						myAgent.addBehaviour(new InformCloudManAndLocalCoordBehaviour(InformCloudManAndLocalCoordBehaviour.INFORM_NODE_ADD, msg.getSender()));
					} else {
						reply.setPerformative(ACLMessage.REFUSE);
					}
				break;
	
				case ACLMessage.CANCEL:
					this.removeFromSupportedNodeList(msg.getSender());
					myAgent.addBehaviour(new InformCloudManAndLocalCoordBehaviour(InformCloudManAndLocalCoordBehaviour.INFORM_NODE_REMOVAL, msg.getSender()));
				break;
			}
			
			myAgent.send(reply);
		} else {
			block();
		}
	}
	
	
	synchronized private boolean canSupport() {
		if(((SupernodeAgent) myAgent).supportedNodes.size() + 1 <= ((SupernodeAgent) myAgent).capacity) {
			return true;
		}
		
		return false;
	}
	
	synchronized private void addToSupportedNodeList(AID nodeAID) {
		SupernodeAgent agent = (SupernodeAgent) myAgent;
		if(agent.supportedNodes.isEmpty()) {
			agent.supportedNodes.add(nodeAID);
			agent.udpServer.setClientsCount(agent.udpServer.getClientsCount() + 1);
		} else {
			if(agent.supportedNodes.stream().filter(node -> node.getName().equalsIgnoreCase(nodeAID.getName())).count() == 0) {
				agent.supportedNodes.add(nodeAID);
				agent.udpServer.setClientsCount(agent.udpServer.getClientsCount() + 1);
			}
		}
	}
	
	private void removeFromSupportedNodeList(AID nodeAID) {
		SupernodeAgent agent = (SupernodeAgent) myAgent;
		if(agent.supportedNodes.size() > 0 && agent.supportedNodes.contains(nodeAID)) {
			agent.supportedNodes.remove(nodeAID);
			System.out.println("Removing client");
			agent.udpServer.setClientsCount(agent.udpServer.getClientsCount() - 1);
			System.out.println(agent.udpServer.getClientsCount());
		}
	}
	
	@Override
	public SupernodeAgent getAgent() {
		return (SupernodeAgent) myAgent;
	}
	
}
