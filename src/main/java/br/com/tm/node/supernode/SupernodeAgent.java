package br.com.tm.node.supernode;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import br.com.tm.node.NodeAgent;
import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.report.Report;
import br.com.tm.report.ReportFactory;
import br.com.tm.server.UDPServer;
import br.com.tm.util.Configuration;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.Property;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class SupernodeAgent extends NodeAgent {

	private static final long serialVersionUID = 1L;
	
	private static final int BASE_CPS = 4096;

	protected int capacity; //same as bandwidth
	protected int bandwidth;
	protected List<AID> supportedNodes;
	/**
	 * {Node name => Rating's list}
	 */
	protected HashMap<String, List<Rating>> supernodeRatingDatabase;
	
	protected double failurePct;
	
	protected UDPServer udpServer;
	
	protected int serverPort;
	
	protected int iterationCount = 1;
	
	protected boolean registeredWithLocalCoordinator = false;
	
	protected Report supernodePerfomanceReport;
	
	protected int baseCapacity;
	
	protected Map<String, Integer[]> satisfactionCache = new HashMap<>();
	
	protected long simulationInitTime;
	
	protected int failureProbability;
	
	@Override
	protected void setup() {
		//super.setup();
		
		report = ReportFactory.getReport(this);
		traceReport = ReportFactory.getTraceReport(this);
		supernodePerfomanceReport = ReportFactory.getReport(getLocalName() +  "_perfomance_report");
		localRatings = new HashMap<>();
		serviceStats = new HashMap<>();
		sharedRatingsMapControl = new HashMap<>();
		supernodeRatingDatabase = new HashMap<>();
		
		supportedNodes = new ArrayList<>();
		getParametersFromArguments("capacity", "serverPort", "startTime", "endTime", "tick", "latitude", "longitude", "trm", "baseCapacity", "bandwidth", "failureProbability");
		
		if(latitude == null || longitude == null) {
			setUpLatLong();
		}
		
		if(baseCapacity == 0) {
			baseCapacity = BASE_CPS;
		}
		
		if(failureProbability == 0) {
			failureProbability = 5;
		}
		
		serverPort = serverPort > 0 ? serverPort : 9876;
		
		//Use this if you pretend to make use of the Jade built in yellow pages service
		//registerSupernodeAgentService();
		
		setupFogService();
		
		addBehaviour(new SupernodeTRMServerBehaviour());
		addBehaviour(new LocalFogServiceBehaviour());
		addBehaviour(new SupernodeStartupBehaviour());
		addBehaviour(new CyclicBehaviour() {
			
			@Override
			public void action() {
				ACLMessage msg = myAgent.receive(MessageTemplate.and(
						MessageTemplate.MatchConversationId("simulation-params-conversation-id"),
						MessageTemplate.and(
								MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE),
								MessageTemplate.MatchPerformative(ACLMessage.INFORM)
							)
				));
				
				if(msg != null) {
					String paramsStr = msg.getContent(); //params = <simulationInitTime>;<startInit>;<startEnd>;<duration>
					if(paramsStr != null && !paramsStr.isEmpty()) {
						String [] params = paramsStr.split(";");
						if(params.length >= 5) {
							((SupernodeAgent) myAgent).simulationInitTime = Long.valueOf(params[0]);
							
							System.out.println("Received params for simulation, duration: " + ((SupernodeAgent) myAgent).simulationInitTime);
							
							iterationDigest();
						} else {
							this.done();
							getAgent().doDelete();
						}
					}
				} else {
					block();
				}
			}
		});
	}
	
	protected void registerSupernodeAgentService() {
		System.out.println("Registering supernode with name: " + this.getLocalName() + ", capacity: " + this.capacity + ", lat/long: " + this.latitude + "/" + this.longitude);
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("supernode");
		sd.setName(this.getName());
		sd.addProperties(new Property("capacity", this.capacity));
		if(this.latitude != null && this.longitude != null) {
			sd.addProperties(new Property("latitude", this.latitude));
			sd.addProperties(new Property("longitude", this.longitude));
		}
		dfd.addServices(sd);
		
		try {
			DFService.register(this, dfd);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}
	
	protected void setupFogService() {
		//this.udpServer = new UDPServer(serverPort, (capacity * 1024) / 8);
		this.udpServer = new UDPServer(serverPort, bandwidth);
		this.udpServer.run();
		System.out.println(this.udpServer.getIp());
	}
	
	protected void getParametersFromArguments(String ... paramatersName) {
		if(getArguments() != null && getArguments().length > 0) {
			for(String parameter : paramatersName) {
				int i = 0;
				while(i < getArguments().length) {
					Object a = getArguments()[i]; 
					if(a instanceof String) {
						if(((String) a).equalsIgnoreCase(parameter)) {
							try {
								if(i + 1 < getArguments().length) {
									if(parameter.equalsIgnoreCase("latitude")) {
										setLatitude(Double.valueOf(getArguments()[i + 1].toString()));
									} else if(parameter.equalsIgnoreCase("longitude")) {
										setLongitude(Double.valueOf(getArguments()[i + 1].toString()));
									} else if(Arrays.stream(this.getClass().getDeclaredFields()).filter(f -> f.getName().equals(parameter)).count() > 0) {
										this.getClass().getDeclaredField(parameter).set(this, dynamicTypeSetting(this.getClass().getDeclaredField(parameter).getType(), getArguments()[i + 1].toString()));
									} else {
										this.getClass().getSuperclass().getDeclaredField(parameter).set(this, dynamicTypeSetting(this.getClass().getDeclaredField(parameter).getType(), getArguments()[i + 1].toString()));
									}
									
									i+=2;
									break;
								}
							} catch (NumberFormatException e) {
								throw new RuntimeException("Invalid arguments for capacity parameter");
							} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
								e.printStackTrace();
								throw new RuntimeException("Invalid arguments for capacity parameter");
							}
						}
					}
					i++;
				}
			}
		}
	}
	
	protected void iterationDigest() {
		if(ThreadLocalRandom.current().nextInt(10) < this.failureProbability) {
			this.udpServer.setCapacity((int)((double)this.bandwidth * (this.failurePct > 0.0 ? this.failurePct : 1.0)));
			System.out.println("Setting capacity server to " + (int)((double)this.bandwidth * (this.failurePct > 0.0 ? this.failurePct : 1.0)));
		} else {
			this.udpServer.setCapacity(this.bandwidth);
			System.out.println("Setting capacity server to " + this.bandwidth);
		}
		
	}
	
	protected Object dynamicTypeSetting(Class fieldType, String value) {
		if(fieldType.getName().equals(Integer.class.getName()) || fieldType.getName().equals("int")) {
			return Integer.valueOf(value);
		} else if(fieldType.getName().equals(Long.class.getName()) || fieldType.getName().equals("long")) {
			return Long.valueOf(value);
		} else if(fieldType.getName().equals(Double.class.getName()) || fieldType.getName().equals("double")) {
			return Double.valueOf(value);
		}
		
		return null;
	}

	protected String getIp() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		return "localhost";
	}
	
	protected void startClock() {
		//Clock ticker
		/*new Thread(new Runnable() {
			@Override
			public void run() {
				Instant i = Instant.ofEpochMilli(new Date().getTime() + Configuration.DAY);
				while(true) {
					while(Clock.tickSeconds(ZoneId.systemDefault()).instant().compareTo(i) < 0) { 	
					
					}
					iterationCount++;
					i = Instant.ofEpochMilli(new Date().getTime() + Configuration.DAY);
					iterationDigest();
				}
			}
		}).start();*/
		iterationDigest();
	}
	
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	

}
