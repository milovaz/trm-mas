package br.com.tm.node;

import java.awt.image.ReplicateScaleFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import br.com.tm.repfogagent.trm.Rating;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class NodeTRMServerBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;
	
	private static final String GET_NODE_RATINGS_CONVERSATION = "get-node-ratings";
	private static final String GET_WITNESS_NODE_RATINGS = "get-witness-node-ratings";
	private static final String GET_NODE_RATINGS_FOR_LOCAL_COORDINATOR = "get-node-ratings-for-lc";
	private static final String GET_NODE_DEVIATION_RATINGS = "get-node-deviation-ratings";
	
	MessageTemplate mtGetRatings = MessageTemplate.and(
		MessageTemplate.and(
			MessageTemplate.or(
				MessageTemplate.or(
					MessageTemplate.MatchConversationId(GET_NODE_RATINGS_CONVERSATION),
					MessageTemplate.MatchConversationId(GET_NODE_DEVIATION_RATINGS)
				),
				MessageTemplate.or(
					MessageTemplate.MatchConversationId(GET_WITNESS_NODE_RATINGS),
					MessageTemplate.MatchConversationId(GET_NODE_RATINGS_FOR_LOCAL_COORDINATOR)
				)
			),
			MessageTemplate.or(
				MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
				MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_QUERY)
			)
		),
		MessageTemplate.or(
			MessageTemplate.MatchPerformative(ACLMessage.QUERY_REF),
			MessageTemplate.MatchPerformative(ACLMessage.REQUEST)
		)
	);
	
	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mtGetRatings);
		try {
			if(msg != null) {
				if(msg.getConversationId().contains(GET_WITNESS_NODE_RATINGS)) {
					String [] supernodesNames = msg.getContent().split("#;#");
					HashMap<String, ArrayList<Rating>> ratingsForSupernodes = null;
					if(getAgent().localRatings != null) {
						ratingsForSupernodes = new HashMap<>();
						for(String supernodeName : supernodesNames) {
							List<Rating> ratings = getAgent().localRatings.get(supernodeName);
							if(ratings != null && !ratings.isEmpty()) {
								ratingsForSupernodes.put(supernodeName, new ArrayList<Rating>(ratings.size() > getAgent().ratingHistorySize ? ratings.subList(ratings.size() - getAgent().ratingHistorySize, ratings.size()) : ratings));
							}
						}
					}
					ACLMessage reply = msg.createReply();
					reply.setPerformative(ACLMessage.INFORM_REF);
					reply.setContentObject(ratingsForSupernodes);
					
					System.out.println("Sending " + ratingsForSupernodes.size() + " ratings to " + msg.getSender().getName());
					
					myAgent.send(reply);
				} else if(msg.getConversationId().contains(GET_NODE_RATINGS_FOR_LOCAL_COORDINATOR)) {
					ArrayList<Rating> ratings = new ArrayList<>(assessLocalCoordinator());
					ACLMessage reply = msg.createReply();
					reply.setPerformative(ACLMessage.INFORM_REF);
					reply.setContentObject(ratings);
					
					myAgent.send(reply);
				} else if(msg.getConversationId().contains(GET_NODE_DEVIATION_RATINGS)) {
					int historySize = Integer.valueOf(msg.getContent());
					System.out.println("Deviation for local coordinator -> " + calculateDeviationForLocalCoordinator(historySize));
				} else {
					sendRatings(msg.createReply());
				}
			} else {
				block();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void sendRatings(ACLMessage reply) throws IOException {
		NodeAgent agent = (NodeAgent) myAgent;
		String server = reply.getSender().getName();
		if(!agent.localRatings.isEmpty() && agent.localRatings.containsKey(server)) {
			List<Rating> filteredRatings = filterAlreadySharedRatings(server, agent.localRatings.get(server));
			if(!filteredRatings.isEmpty()) {
				reply.setPerformative(ACLMessage.INFORM);
				reply.setContentObject(new ArrayList<>(filteredRatings));
			} else {
				reply.setPerformative(ACLMessage.FAILURE);
			}
		} else {
			reply.setPerformative(ACLMessage.FAILURE);
		}

		myAgent.send(reply);
	}
	
	private List<Rating> filterAlreadySharedRatings(String supernode, List<Rating> ratings) {
		NodeAgent agent = (NodeAgent) myAgent;
		List<Rating> filteredRatings = new ArrayList<>();
		if(agent.sharedRatingsMapControl.containsKey(supernode)) {
			Date lastSharedDate = agent.sharedRatingsMapControl.get(supernode);
			for(Rating rating : ratings) {
				if(rating.getDate().compareTo(lastSharedDate) > 0) {
					filteredRatings.add(rating);
				}
			}
		} else {
			agent.sharedRatingsMapControl.put(supernode, ratings.get(ratings.size() - 1).getDate());
			filteredRatings = ratings;
		}
		
		return filteredRatings;
	}

	private List<Rating> assessLocalCoordinator() {
		List<Rating> ratings = new ArrayList<>();
		if(getAgent().lcStats != null) {
			if(getAgent().lcStats.getCountInterations() > getAgent().localCoordinatorIterationRatingThreshold) {
				double availlabilityRatingValue = 1;
				if((double)((double)getAgent().lcStats.getCountDownTimes() / (double)getAgent().lcStats.getCountInterations()) > getAgent().localCoodinatorDownThreshold) {  
					availlabilityRatingValue -= ((double)((double)getAgent().lcStats.getCountDownTimes() / (double)getAgent().lcStats.getCountInterations()) - getAgent().localCoodinatorDownThreshold);
				}
				
				System.out.println("Local coordinator stats: " + (double)getAgent().lcStats.getCountDownTimes() + "/" + (double)getAgent().lcStats.getCountInterations());
				
				ratings.add(new Rating(getAgent().getName(), getAgent().localCoordinator.getName(), availlabilityRatingValue, availlabilityRatingValue, getAgent().interationCount, "availability", new Date()));
			}
		}
		
		return ratings;
	}
	
	private double calculateDeviationForLocalCoordinator(int historySize) {
		List<Rating> organizedRatings = new ArrayList<>();
		
		for(List<Rating> l : getAgent().localRatings.values()) {
			organizedRatings.addAll(l);
		}
		
		organizedRatings.sort(new Comparator<Rating>() {
			@Override
			public int compare(Rating o1, Rating o2) {
				return Integer.valueOf(o1.getIteration()).compareTo(o2.getIteration());
			}
		});
		int iterationInit = organizedRatings.get(organizedRatings.size() - 1).getIteration() - historySize;
		int lastIteration = organizedRatings.get(organizedRatings.size() - 1).getIteration();
		iterationInit  = iterationInit < 0 ? 0 : iterationInit;
		List<Rating> filteredRatings = new ArrayList<>();
		
		for(Rating r : organizedRatings) {
			if(r.getIteration() >= iterationInit) {
				filteredRatings.add(r);
			}
		}
		
		double sumNumerator = 0;
		double sumDenominator = 0;
		for(int i = 0; i < organizedRatings.size(); i++) {
			for(int j = 0; j < organizedRatings.size(); j++) {
				if(i != j) {
					sumNumerator += Math.abs(organizedRatings.get(i).getValue() - organizedRatings.get(j).getValue()) * deviationWeight(lastIteration, organizedRatings.get(i).getIteration());
					sumDenominator += deviationWeight(lastIteration, organizedRatings.get(i).getIteration());
				}
			}
		}
		
		return 1 - ((1.0/2.0) * (sumNumerator / sumDenominator));
	}
	
	private double deviationWeight(int currentIteration, int ti) {
		return Math.exp(Math.abs(currentIteration - ti) / ((-1) * (7 / Math.log(0.5))));
	}
	
	@Override
	public NodeAgent getAgent() {
		return (NodeAgent) myAgent;
	}
	
}
