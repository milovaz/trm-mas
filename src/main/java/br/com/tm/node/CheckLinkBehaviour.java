package br.com.tm.node;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import br.com.tm.client.UDPProbeClient;
import br.com.tm.util.ServerInfo;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;

public class CheckLinkBehaviour extends Behaviour {

	private static final long serialVersionUID = 1L;

	private static final int SEND_CHECK_MESSAGES = 0;
	private static final int WAIT_FOR_ANSWER = 1;
	private static final int END = 2;
	
	ServerInfo serverInfo;
	BlockingQueue<Object> queue;
	Thread udpClientThread;
	
	private int step = SEND_CHECK_MESSAGES;
	
	public CheckLinkBehaviour(ServerInfo serverInfo) {
		this.serverInfo = serverInfo;
	}
	
	@Override
	public void action() {
		switch (step) {
			case SEND_CHECK_MESSAGES:
				queue = new LinkedBlockingQueue<Object>();
				UDPProbeClient udpProbeClient = new UDPProbeClient(getAgent().getName(), serverInfo.getSever(), serverInfo.getServerIp(), serverInfo.getPort(), getAgent().latencyRequirement, queue);
				udpClientThread = new Thread(udpProbeClient);
				udpClientThread.start();
			break;

			case WAIT_FOR_ANSWER:
				consume(this.queue.poll());
			break;
		}
	}
	
	private void consume(Object queueValue) {
		if(queueValue != null) {
			if(((String) queueValue).toLowerCase().contains("stopped") || ((String) queueValue).toLowerCase().contains("stoping")) {
				String [] mParts = ((String) queueValue).split("#;#");
				System.out.println("Received stopped probe for " + mParts[1]);
				udpClientThread.stop();
				getAgent().cloudTransmissionDelay.put(serverInfo.getSever(), Double.valueOf(mParts[2]));
			}
		}
	}

	@Override
	public NodeAgent getAgent() {
		return (NodeAgent) myAgent;
	}
	
	@Override
	public boolean done() {
		return step == END;
	}

}
