package br.com.tm.node;

public enum NodeStatus {
	INACTIVE(0, "inactive"),
	SEARCHING_SERVER(1, "searching_server"),
	CONNECTING(2, "connecting"),
	READY(3, "ready"),
	STARTED(4, "started");
	
	public int value;
	public String description;
	
	private NodeStatus(int value, String description) {
		this.value = value;
		this.description = description;
	}
}
