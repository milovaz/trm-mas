package br.com.tm.node;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

public class FogSimulationBehaviour extends TickerBehaviour {

	private static final long serialVersionUID = 1L;
	
	private boolean started = true;
	
	public FogSimulationBehaviour(Agent a, long period) {
		super(a, period);
	}

	@Override
	protected void onTick() {
		if(this.started) {
			NodeAgent agentNode = (NodeAgent) myAgent;
			if(agentNode.udpClient != null) {
				agentNode.udpClient.stop();
				agentNode.udpClient = null;
			}
			myAgent.doSuspend();
		} else {
			myAgent.doActivate();
		}
		this.started = !this.started;
	}

}
