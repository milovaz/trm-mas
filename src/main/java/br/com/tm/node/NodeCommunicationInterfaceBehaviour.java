package br.com.tm.node;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import br.com.tm.stream.MessageStats;
import jade.core.behaviours.CyclicBehaviour;

public class NodeCommunicationInterfaceBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;

	private BlockingQueue<Object> clientQueue;
	
	public NodeCommunicationInterfaceBehaviour(BlockingQueue<Object> clientQueue) {
		this.clientQueue = clientQueue;
	}
	
	@Override
	public void action() {
		consume(this.clientQueue.poll());
	}

	private void consume(Object queueValue) {
		if(queueValue != null) {
			if(queueValue instanceof String) {
				System.out.println("Node received " + queueValue);
				String value = (String) queueValue;
				if(value.toLowerCase().contains("started") || value.toLowerCase().contains("starting")) {
					getAgent().nodeStatus = NodeStatus.STARTED;
				} else if(value.toLowerCase().contains("stopped") || value.toLowerCase().contains("stoping")) {
					getAgent().nodeStatus = NodeStatus.INACTIVE;
					getAgent().addBehaviour(new TearDownBehaviour(true));
				}
			} else if(queueValue instanceof MessageStats) {
				if(getAgent().serviceStats.containsKey(((MessageStats) queueValue).server)) {
					getAgent().serviceStats.get(((MessageStats) queueValue).server).add(((MessageStats) queueValue));
				} else {
					List<MessageStats> lista = new ArrayList<>();
					lista.add(((MessageStats) queueValue));
					getAgent().serviceStats.put(((MessageStats) queueValue).server, lista);
				}
					
			}
		} else {
			block(100);
		}
	}
	
	@Override
	public NodeAgent getAgent() {
		return (NodeAgent) myAgent;
	}
	
}
