package br.com.tm.node;

import java.io.IOException;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import br.com.tm.client.UDPClient;
import br.com.tm.client.UDPProbeClient;
import br.com.tm.repfogagent.trm.FIREtrm;
import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.repfogagent.trm.components.FIREBaseComponent;
import br.com.tm.repfogagent.trm.components.InteractionTrustComponent;
import br.com.tm.repfogagent.trm.components.WitnessReputationComponent;
import br.com.tm.repfogagent.util.TRMHelper;
import br.com.tm.repfogagent.util.Util;
import br.com.tm.stream.MessageStats;
import br.com.tm.util.Configuration;
import br.com.tm.util.FogServiceModel;
import br.com.tm.util.GeoUtil;
import br.com.tm.util.ServerInfo;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class SelectServerBehaviour extends Behaviour {

	private static final long serialVersionUID = 1L;

	private static final String GET_SUPERNODE_CERTIFICATE_CONVERSATION_ID = "get-supernode-certificate";
	private static final String ADD_TO_SUPPORTED_LIST = "add-to-supported-list";
	private static final String GET_SUPERNODE_RATINGS_CONVERSATION_ID = "get-supernode-ratings";
	private static final String GET_SUPERNODE_REFERALS = "get-supernode-referals";
	
	private static final int FILTER_SUPERNODES_BY_TRANSMISSION_DELAY = 0;
	private static final int QUERY_SUPERNODES_CERTIFICATES = 1;
	private static final int RECEIVE_SUPERNODES_CERTIFICATES_AND_CALCULATE_TRM = 2;
	private static final int RECEIVE_WITNESS_LIST = 3;
	private static final int RECEIVE_WITNESS_RATINGS_AND_CALCULATE_TRM = 4;
	private static final int CALCULATE_TRM = 5;
	private static final int CALCULATE_UTILITY = 6;
	private static final int ADD_TO_SUPERNODE = 7;
	private static final int WAIT_FOR_SERVER_ACCEPTANCE = 8;
	private static final int INFORM_CLOUD_AND_LOCAL_COORDINATOR = 9;
	private static final int START_CLIENT_SERVICE = 10;
	private static final int RAISE_ERROR = 11;
	private static final int END = 12;
	
	MessageTemplate mt = MessageTemplate.and(
			MessageTemplate.MatchConversationId(GET_SUPERNODE_CERTIFICATE_CONVERSATION_ID),
			MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST)
	);
	
	private List<String> selectedSupernodesList;
	private List<ServerInfo> supernodesList;   
	private Map<String, Double[]> trmValuesMap;
	private Map<String, Map<String, List<Rating>>> witnessRatingsCache;
	private TRMHelper trmHelper;
	private int countReplies = 0;
	private int witnessReplies = 0;
	
	private AID serverCandidate;
	private int serverCandidateType;
	
	private String errorMessage;
	
	private int countTrysWitness = 3;
	
	private int step = 0;
	
	private boolean waintingForTransmissionCheck = false;
	
	private String certificateReplyWhith;
	
	public SelectServerBehaviour(List<ServerInfo> supernodesList) {
		this.supernodesList = supernodesList;
		this.trmHelper = TRMHelper.getInstance();
		this.serverCandidate = null;
		this.trmValuesMap = new HashMap<>();
		this.witnessRatingsCache = new HashMap<>();
	}
	
	@Override
	public void action() {
		switch (step) {
			case FILTER_SUPERNODES_BY_TRANSMISSION_DELAY:
				if(!getAgent().cloudTransmissionDelay.isEmpty()) {
					System.out.println("Waiting for cloud transmission delay");
				} else {
					if(this.supernodesList != null && !this.supernodesList.isEmpty()) {
						
						//Filter by transmission delay
						//BlockingQueue<String> localQueue = new SynchronousQueue<String>();
						//filterSupernodesByTransmissionDelay(localQueue);
						filterSupernodesByPresumableTransmissionDelay();
						
						System.out.println("transmisison check ended");
						
						if(getAgent().trm != Configuration.RANDOM && getAgent().interationCount > getAgent().iterationTRMThreshold) {
							step = QUERY_SUPERNODES_CERTIFICATES;
						} else{
							System.out.println("Randomly selecting supernode");
							this.selectedSupernodesList = new ArrayList<ServerInfo>(this.supernodesList).stream().map(s -> s.getSever()).collect(Collectors.toList());
							Collections.shuffle(this.selectedSupernodesList);
							step = ADD_TO_SUPERNODE;
						}
					} else {
						step = ADD_TO_SUPERNODE;
					}
					
					getAgent().traceReport.add("FILTERED_SUPERNODES_LIST;" + Clock.systemUTC().millis());
				}
			break;
			
			case QUERY_SUPERNODES_CERTIFICATES:
				//Query for certified reputation values
				if(getAgent().nodeTRMApproach == NodeTRMApproach.CR_FROM_SUPERNODE) {
					System.out.println(myAgent.getName() + " sending message to get certificate....");
					ACLMessage requestServiceMessage = new ACLMessage(ACLMessage.REQUEST);
					requestServiceMessage.setConversationId(GET_SUPERNODE_CERTIFICATE_CONVERSATION_ID);
					requestServiceMessage.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
					
					if(this.supernodesList != null) {
						for(ServerInfo serverInfo : this.supernodesList) {
							System.out.println("Sending certificate request to " + serverInfo.getAid().getName());
							requestServiceMessage.addReceiver(serverInfo.getAid());
						}
					}
					
					certificateReplyWhith = "get-sn-cr-"+System.currentTimeMillis();
					requestServiceMessage.setReplyWith(certificateReplyWhith);
					
					myAgent.send(requestServiceMessage);
					
					getAgent().traceReport.add("REQUESTED_SUPERNODES_CERTIFICATES;" + Clock.systemUTC().millis());
					
					step = RECEIVE_SUPERNODES_CERTIFICATES_AND_CALCULATE_TRM;
					
				} else if(getAgent().nodeTRMApproach == NodeTRMApproach.CR_FROM_LOCAL_COORDINATOR || getAgent().nodeTRMApproach == NodeTRMApproach.WR_FROM_LOCAL_COORDINATOR) {
					ACLMessage requestServiceMessage = new ACLMessage(ACLMessage.QUERY_REF);
					
					if(getAgent().nodeTRMApproach == NodeTRMApproach.CR_FROM_LOCAL_COORDINATOR) {
						requestServiceMessage.setConversationId(GET_SUPERNODE_RATINGS_CONVERSATION_ID);
					} else if(getAgent().nodeTRMApproach == NodeTRMApproach.WR_FROM_LOCAL_COORDINATOR) {
						List<AID> supernodesCandidatesReferrals = getAgent().getSupernodeReferrals();
						if(supernodesCandidatesReferrals != null && !supernodesCandidatesReferrals.isEmpty()) {
							System.out.println("Sending query to referrals ");
							supernodesCandidatesReferrals.forEach(s -> System.out.print(s.getName() + ";"));
							
							ACLMessage requestRatingsMessage = new ACLMessage(ACLMessage.QUERY_REF);
							requestRatingsMessage.setConversationId("get-witness-node-ratings");
							requestRatingsMessage.setProtocol(FIPANames.InteractionProtocol.FIPA_QUERY);
							
							String content = "";
							for(ServerInfo superNode : this.supernodesList) {
								content += content.length() > 0 ? "#;#" + superNode.getSever() : superNode.getSever(); 
							}
								
							requestRatingsMessage.setContent(content);
							
							for(AID referalAID : supernodesCandidatesReferrals) {
								requestRatingsMessage.addReceiver(referalAID);
								witnessReplies++;
							}
							
							witnessRatingsCache = new HashMap<>();
							
							myAgent.send(requestRatingsMessage);
							
							getAgent().traceReport.add("REQUESTED_WITNESS_RATINGS;" + Clock.systemUTC().millis());
							
							step = RECEIVE_WITNESS_RATINGS_AND_CALCULATE_TRM;
						} else {
							System.out.println("Randomly selecting supernode");
							this.selectedSupernodesList = new ArrayList<ServerInfo>(this.supernodesList).stream().map(s -> s.getSever()).collect(Collectors.toList());
							Collections.shuffle(this.selectedSupernodesList);
							step = ADD_TO_SUPERNODE;
						}
					}
					
				} else if(getAgent().nodeTRMApproach == NodeTRMApproach.WR_FROM_NODES) {
					// TODO
				}
				
			break;
			
			case RECEIVE_WITNESS_LIST:
				try {
					ACLMessage witnessListreply = myAgent.receive(MessageTemplate.and(
							MessageTemplate.MatchConversationId(GET_SUPERNODE_REFERALS),
							MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_QUERY)
					));
					if(witnessListreply != null) {
						System.out.println(myAgent.getName() + " received referrals from local coordinator");
						if(witnessListreply.getContentObject() != null) {
							
							ACLMessage requestRatingsMessage = new ACLMessage(ACLMessage.QUERY_REF);
							requestRatingsMessage.setConversationId("get-witness-node-ratings");
							requestRatingsMessage.setProtocol(FIPANames.InteractionProtocol.FIPA_QUERY);
							
							String content = "";
							for(ServerInfo superNode : this.supernodesList) {
								content += content.length() > 0 ? "#;#" + superNode.getSever() : superNode.getSever(); 
							}
								
							requestRatingsMessage.setContent(content);
							
							List<AID> referals = (List<AID>) witnessListreply.getContentObject();
							if(referals != null && !referals.isEmpty()) {
								for(AID referalAID : referals) {
									requestRatingsMessage.addReceiver(referalAID);
									witnessReplies++;
								}
							}
							
							witnessRatingsCache = new HashMap<>();
							
							myAgent.send(requestRatingsMessage);
							
							countTrysWitness = 3;
							
							getAgent().traceReport.add("REQUESTED_WITNESS_RATINGS;" + Clock.systemUTC().millis());
							
							step = RECEIVE_WITNESS_RATINGS_AND_CALCULATE_TRM;
						} else {
							System.out.println("Randomly selecting supernode");
							this.selectedSupernodesList = new ArrayList<ServerInfo>(this.supernodesList).stream().map(s -> s.getSever()).collect(Collectors.toList());
							Collections.shuffle(this.selectedSupernodesList);
							step = ADD_TO_SUPERNODE;
						}
					} else {
						block();
					}
				} catch (UnreadableException e) {
					e.printStackTrace();
				}
			break;
			
			case RECEIVE_WITNESS_RATINGS_AND_CALCULATE_TRM:
				ACLMessage witnessRatingsReply = myAgent.receive(MessageTemplate.and(
						MessageTemplate.MatchConversationId("get-witness-node-ratings"),
						MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_QUERY)
				));
				if(witnessRatingsReply != null) {
					try {
						if(witnessRatingsReply.hasByteSequenceContent()) {
							Map<String, List<Rating>> witnessNodeRatingDatabase = (Map<String, List<Rating>>) witnessRatingsReply.getContentObject();
							if(!witnessNodeRatingDatabase.isEmpty()) {
								witnessRatingsCache.put(witnessRatingsReply.getSender().getName(), witnessNodeRatingDatabase);
								System.out.println(myAgent.getName() + " received ratings from " + witnessRatingsReply.getSender().getLocalName());
							}
						}
						
						witnessReplies--;
						if(witnessReplies == 0) {
							getAgent().traceReport.add("RECEIVED_SUPERNODES_RATINGS_FROM_REFERRALS;" + Clock.systemUTC().millis());
							
							// Calculate TRM
							calculateTRMForSupernodes(witnessRatingsCache);
							
							step = CALCULATE_UTILITY;
						}
					} catch (UnreadableException e) {
						e.printStackTrace();
					}
				} else {
					countTrysWitness--;
					if(countTrysWitness == 0 && witnessRatingsCache.size() >= (0.7 * (witnessReplies + witnessRatingsCache.size()))) {
						getAgent().traceReport.add("RECEIVED_SUPERNODES_RATINGS_FROM_REFERRALS;" + Clock.systemUTC().millis());
						
						System.out.println("Received " + (0.7 * (witnessReplies + witnessRatingsCache.size())));
						
						// Calculate TRM
						calculateTRMForSupernodes(witnessRatingsCache);
						
						step = CALCULATE_UTILITY;
					} else {
						System.out.println("Randomly selecting supernode");
						this.selectedSupernodesList = new ArrayList<ServerInfo>(this.supernodesList).stream().map(s -> s.getSever()).collect(Collectors.toList());
						Collections.shuffle(this.selectedSupernodesList);
						step = ADD_TO_SUPERNODE;
					}
					block(1000);
				}
			break;
			
			case RECEIVE_SUPERNODES_CERTIFICATES_AND_CALCULATE_TRM:
				try {
					ACLMessage reply = myAgent.receive(MessageTemplate.and(
							MessageTemplate.MatchInReplyTo(certificateReplyWhith),
							MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST)
					));
					if(reply != null) {
						System.out.println(myAgent.getName() + " received certificate from supernode");
						if(reply.getContentObject() != null) {
							Map<String, List<Rating>> supernodeRatingDatabase = (Map<String, List<Rating>>)reply.getContentObject();
							System.out.println(myAgent.getName() + " received certificate from supernode");
							
							for(Entry<String, List<Rating>> entry : supernodeRatingDatabase.entrySet()) {
								Map<String, List<Rating>> mapAux = new HashMap<>();
								mapAux.put(reply.getSender().getName(), entry.getValue());
								witnessRatingsCache.put(entry.getKey(), mapAux);
							}
							
							/*if(supernodeRatingDatabase != null && !supernodeRatingDatabase.isEmpty()) {
								List<Rating> ratingsForSupernode =  ((NodeAgent) myAgent).localRatings.get(reply.getSender().getName());
								ratingsForSupernode = ratingsForSupernode == null ? new ArrayList<Rating>() : ratingsForSupernode;
								//ratingsForSupernode.forEach(r -> System.out.println(r.getServerName() + " - " + r.getValue()));
								
								Double [] trmValue = trmHelper.calcSuperNodesCertifiedTrust(myAgent.getName(), reply.getSender().getName(), ratingsForSupernode, supernodeRatingDatabase, new Date(), getAgent().interationCount);
								trmValuesMap.put(reply.getSender().getName(), trmValue);
								
							}*/
						}
						countReplies++;
						if(countReplies >= this.supernodesList.size()) {
							getAgent().traceReport.add("RECEIVED_SUPERNODES_CERTIFICATES;" + Clock.systemUTC().millis());
							
							calculateTRMForSupernodes(witnessRatingsCache);
							
							step = CALCULATE_UTILITY;
						}
					} else {
						block();
					}
				} catch (UnreadableException e) {
					e.printStackTrace();
				}
			break;
			
			case CALCULATE_TRM:
			break;
			
			// TODO Tratar caso onde trmValuesMap está vazio ou é nulo
			case CALCULATE_UTILITY:
				if(this.trmValuesMap != null && !this.trmValuesMap.isEmpty()) {
					Map<String, Double> superNodesUtilities = ((NodeAgent) myAgent).calculateUtilityForSupernodes(this.trmValuesMap);
					superNodesUtilities = Util.sortByValue(superNodesUtilities);
					
					Map<String, Double> cloudUtilities = getAgent().calculateUtilityForClouds();
					if(cloudUtilities.size() > 0) {
						cloudUtilities = Util.sortByValue(cloudUtilities);
					}
					
					ArrayList<String> supernodesList = new ArrayList<>();
					for(Entry<String, Double> entryCloud : cloudUtilities.entrySet()) {
						System.out.println("Utility " + entryCloud.getKey() + " - " + entryCloud.getValue());
						for(Entry<String, Double> entrySupernode : superNodesUtilities.entrySet()) {
							System.out.println("Utility " + entrySupernode.getKey() + " - " + entrySupernode.getValue());
							if(entryCloud.getValue().compareTo(entrySupernode.getValue()) >= 0) {
								supernodesList.add(entryCloud.getKey());
							} else {
								supernodesList.add(entrySupernode.getKey());
							}
						}
					}
					
					this.selectedSupernodesList = supernodesList;
					step = ADD_TO_SUPERNODE;
				} else {
					System.out.println("EMPTY TRM");
					addToCloudServer(getClosestCloudManager());
					step = WAIT_FOR_SERVER_ACCEPTANCE;
				}
			break;
			
			case ADD_TO_SUPERNODE:
				getAgent().nodeStatus = NodeStatus.CONNECTING;
				if(this.selectedSupernodesList != null && !this.selectedSupernodesList.isEmpty()) {
					AID supernodeAID = null;
					try {
						supernodeAID = this.supernodesList.stream().filter(sn -> sn.getAid().getName().equalsIgnoreCase(this.selectedSupernodesList.get(0))).findFirst().map(sn -> sn.getAid()).get();
					} catch (NoSuchElementException e) { 
					}
					
					if(supernodeAID != null) {
						this.selectedSupernodesList.remove(0);
						
						System.out.println("Sending service add to supernode request....");
						
						ACLMessage requestToBeSupportedMessage = new ACLMessage(ACLMessage.SUBSCRIBE);
						requestToBeSupportedMessage.setConversationId(ADD_TO_SUPPORTED_LIST);
						requestToBeSupportedMessage.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
						requestToBeSupportedMessage.addReceiver(supernodeAID);
						requestToBeSupportedMessage.setReplyWith("add-to-supported-list-"+System.currentTimeMillis());
						myAgent.send(requestToBeSupportedMessage);

						this.serverCandidate = supernodeAID;
						this.serverCandidateType = NodeAgent.SUPERNODE;
						
						getAgent().traceReport.add("REQUEST_SERVICE_TO_SUPERNODE;" + Clock.systemUTC().millis());
					} else {
						addToCloudServer(getClosestCloudManager());
					}
				} else {
					addToCloudServer(getClosestCloudManager());
				}
				
				step = WAIT_FOR_SERVER_ACCEPTANCE;
			break;
			
			case WAIT_FOR_SERVER_ACCEPTANCE:
				ACLMessage msg = myAgent.receive(MessageTemplate.and(
							MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE),
							MessageTemplate.MatchConversationId(ADD_TO_SUPPORTED_LIST)
						));
				
				if(msg != null) {
					if(msg.getPerformative() == ACLMessage.AGREE) {
						System.out.println(myAgent.getLocalName() + " received AGREE for subscription from " + msg.getSender().getLocalName());
						((NodeAgent) myAgent).server = this.serverCandidate;
						((NodeAgent) myAgent).serverType = this.serverCandidateType;
						if(this.serverCandidateType == NodeAgent.SUPERNODE) {
							if(this.supernodesList != null && !this.supernodesList.isEmpty()) {
								((NodeAgent) myAgent).serverInfo = this.supernodesList.stream().filter(si -> si.getSever().equals(this.serverCandidate.getName())).findFirst().get();
								
								System.out.println("Connected to " + ((NodeAgent) myAgent).serverInfo.getSever() + ", type " + this.serverCandidateType + ", port " + ((NodeAgent) myAgent).serverInfo.getPort());
								
								getAgent().traceReport.add("RECEIVED_ACCEPTANCE_FROM_SUPERNODE;" + Clock.systemUTC().millis() + ";" + ((NodeAgent) myAgent).serverInfo.getSever());
								
								step = START_CLIENT_SERVICE;
							} else {
								addToCloudServer(getClosestCloudManager());
								
								step = WAIT_FOR_SERVER_ACCEPTANCE;
							}
						} else {
							for(ServerInfo si : getAgent().cloudManagersServerInfo) {
								if(si.getAid().getName().equals(((NodeAgent) myAgent).server.getName())) {
									getAgent().serverInfo = si;
									break;
								}
							}
							 
							step = START_CLIENT_SERVICE;
						}
					} else if(msg.getPerformative() == ACLMessage.REFUSE) {
						System.out.println(myAgent.getLocalName() + " received REFUSE for subscription from " + msg.getSender().getLocalName());
						getAgent().traceReport.add(myAgent.getLocalName() + "received REFUSE for subscription from " + msg.getSender().getLocalName());
						
						this.serverCandidate = null;
						
						if(this.serverCandidateType == NodeAgent.SUPERNODE) {
							this.serverCandidateType = -1;
							step = ADD_TO_SUPERNODE;
						} else if(this.serverCandidateType == NodeAgent.CLOUD) {
							this.errorMessage = "Cloud server is not responding. Shutting client down.";
							step = RAISE_ERROR;
						}
					}
				} else {
					block();
				}
				
			break;
			
			case START_CLIENT_SERVICE:
				//myAgent.addBehaviour(new StartupBehaviour());
				((NodeAgent) myAgent).nodeStatus = NodeStatus.READY;
				step = END;
			break;
			
			case RAISE_ERROR:
				throw new RuntimeException(this.errorMessage);
		}
	}
	
	private void addToCloudServer(AID cloudServerAID) {
		System.out.println("Sending add to supernode request to CLOUD");
		
		ACLMessage requestToBeSupportedMessage = new ACLMessage(ACLMessage.SUBSCRIBE);
		requestToBeSupportedMessage.setConversationId(ADD_TO_SUPPORTED_LIST);
		requestToBeSupportedMessage.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
		requestToBeSupportedMessage.addReceiver(cloudServerAID);
		requestToBeSupportedMessage.setReplyWith("add-to-supported-list-"+System.currentTimeMillis());
		myAgent.send(requestToBeSupportedMessage);
		this.serverCandidate = cloudServerAID;
		this.serverCandidateType = NodeAgent.CLOUD;
		
		getAgent().traceReport.add("REQUEST_SERVICE_TO_CLOUD;" + Clock.systemUTC().millis());
	}
	
	private void calculateTRMForSupernodes(Map<String, Map<String, List<Rating>>> witnessRatingsCache) {
		for(ServerInfo superNode : this.supernodesList) {
			List<Rating> ratingsForSupernode =  ((NodeAgent) myAgent).localRatings.get(superNode.getSever());
			ratingsForSupernode = ratingsForSupernode == null ? new ArrayList<Rating>() : ratingsForSupernode;
			
			Map<String, List<Rating>> supernodeRatingDatabase = new HashMap<>();
			for(Entry<String, Map<String, List<Rating>>> e1 : witnessRatingsCache.entrySet()) {
				if(e1.getValue().get(superNode.getSever()) != null && !e1.getValue().get(superNode.getSever()).isEmpty()) {
					supernodeRatingDatabase.put(e1.getKey(), e1.getValue().get(superNode.getSever()));
				}
			}
			
			List<FIREBaseComponent> fireComponents = new ArrayList<FIREBaseComponent>();
			
			if(ratingsForSupernode.size() > 0) {
				FIREBaseComponent interactionTrust = calculateInteractionTrust(ratingsForSupernode);
				fireComponents.add(interactionTrust);
			}
			
			FIREBaseComponent witnessReputation = calculateWitnessReputation(supernodeRatingDatabase, ratingsForSupernode);
			
			FIREtrm firEtrm = new FIREtrm(fireComponents);
			Double fireValue = firEtrm.calculate();
			Double overallReliability = firEtrm.reliability();
			
			trmValuesMap.put(superNode.getSever(), new Double [] {fireValue, overallReliability});
		}
	}
	
	private FIREBaseComponent calculateWitnessReputation(Map<String, List<Rating>> ratingsPerNode, List<Rating> localRatings) {
		WitnessReputationComponent witnessReputationComponent = new WitnessReputationComponent(0, 0.4, 1.0, ratingsPerNode, localRatings);
		
		List<Rating> supportingRatings = new ArrayList<>();
		ratingsPerNode.values().forEach(r -> supportingRatings.addAll(r));
		
		double witnessValue = witnessReputationComponent.calculate(supportingRatings, getAgent().interationCount);
		double reliabilityWR = witnessReputationComponent.reliability(supportingRatings);
		
		witnessReputationComponent.setCalculatedValue(witnessValue);
		witnessReputationComponent.setCalculatedReliability(reliabilityWR);
		
		return witnessReputationComponent;
	}
	
	private FIREBaseComponent calculateInteractionTrust(List<Rating> localRatings) {
		InteractionTrustComponent interactionTrustComponent = new InteractionTrustComponent(0, 0.8);
		
		double interactionTrustValue = interactionTrustComponent.calculate(localRatings, getAgent().interationCount);
		double reliabilityIT = interactionTrustComponent.reliability(localRatings);
		
		interactionTrustComponent.setCalculatedValue(interactionTrustValue);
		interactionTrustComponent.setCalculatedReliability(reliabilityIT);
		
		return interactionTrustComponent;
	}
	
	/**
	 * 
	 * @return In this version the cloud manager is randomly selected from the cloud manager list
	 */
	private AID getClosestCloudManager() {
		NodeAgent agent = ((NodeAgent) myAgent);
		return agent.cloudManagers[ThreadLocalRandom.current().nextInt(agent.cloudManagers.length)];
	}
	
	private AID loadAID(String name, String type) {
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType(type);
		sd.setName(name);
		template.addServices(sd);
		
		try {
			DFAgentDescription[] result = DFService.search(myAgent, template);
			for (int i = 0; i < result.length; ++i) {
				if(result[i].getName().getName().equalsIgnoreCase(name)) {
					return result[i].getName(); 
				}
			}	
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private void filterSupernodesByPresumableTransmissionDelay() {
		GeoUtil geoUtil = new GeoUtil();
		for(int i = 0; i < this.supernodesList.size(); i++) {
			ServerInfo serverInfo = this.supernodesList.get(i);
			int latency = geoUtil.kmToLatencyRatio(geoUtil.distance(getAgent().getGeolocation(), serverInfo.getGeolocation()));
			if(latency > getAgent().latencyRequirement) {
				this.supernodesList.remove(i);
			}
		}
	}
	
	private void filterSupernodesByTransmissionDelay(BlockingQueue<String> localQueue) {
		BlockingQueue<Object> parallelClientQueue = new SynchronousQueue<Object>();
		Map<String, Thread> probeClientThreads = new HashMap<>(); 
		waintingForTransmissionCheck = true;
		for(ServerInfo serverInfo : this.supernodesList) {
			UDPProbeClient udpProbeClient = new UDPProbeClient(getAgent().getName(), serverInfo.getSever(), serverInfo.getServerIp(), serverInfo.getPort(), getAgent().latencyRequirement, parallelClientQueue);
			Thread udpClientThread = new Thread(udpProbeClient);
			udpClientThread.start();
			System.out.println("Filtering " + serverInfo.getSever());
			probeClientThreads.put(serverInfo.getSever(), udpClientThread);
		}
		

		try {
			while(!probeClientThreads.isEmpty()) {
				Object queueValue = parallelClientQueue.take();
				if(queueValue instanceof String) {
					if(((String) queueValue).toLowerCase().contains("stopped") || ((String) queueValue).toLowerCase().contains("stoping")) {
						String [] mParts = ((String) queueValue).split("#;#");
						System.out.println("Received stopped probe for " + mParts[1]);
						if(supernodesList.size() > 0 && probeClientThreads.size() > 0) {
							if(Integer.valueOf(mParts[2]) > getAgent().latencyRequirement) { 
								supernodesList.removeIf(s -> s.getSever().equals(mParts[1]));
							}
						}
						probeClientThreads.get(mParts[1]).stop();
						probeClientThreads.remove(mParts[1]);
					}
				}
			}
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}
	
	@Override
	public boolean done() {
		return step == END;
	}
	
	@Override
	public NodeAgent getAgent() {
		return (NodeAgent) myAgent;
	}
	
}
