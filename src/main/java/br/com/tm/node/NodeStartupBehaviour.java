package br.com.tm.node;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

import br.com.tm.node.supernode.SupernodeAgent;
import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.util.CloudConnectResponse;
import br.com.tm.util.Configuration;
import br.com.tm.util.FogServiceModel;
import br.com.tm.util.ServerInfo;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.Property;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;


public class NodeStartupBehaviour extends Behaviour {

	private static final long serialVersionUID = 1L;
	
	private static final String SIMULATION_PARAMAS_CONVERSATION_ID = "simulation-params-conversation-id";
	private static final String NODE_REGISTRATION_CONVERSATION_ID = "node-registration-conversation-id";
	private static final String SIMULATION_ITERATION_CONTROL_CONVERSATION = "sim-iteration-control";
	
	MessageTemplate mtParamsMessage = MessageTemplate.and(
			MessageTemplate.MatchConversationId(SIMULATION_PARAMAS_CONVERSATION_ID),
			MessageTemplate.and(
					MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE),
					MessageTemplate.MatchPerformative(ACLMessage.INFORM)
				)
	);
	
	MessageTemplate mtNodeRegistryMessage = MessageTemplate.and(
			MessageTemplate.MatchConversationId(NODE_REGISTRATION_CONVERSATION_ID),
			MessageTemplate.and(
					MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE),
					MessageTemplate.or(
						MessageTemplate.MatchPerformative(ACLMessage.AGREE),
						MessageTemplate.MatchPerformative(ACLMessage.REFUSE)
					)
				)
	);

	/**
	 * Steps for the FSM - Finite state machine
	 */
	private static final int FIND_CLOUD_MANAGER = 0;
	private static final int CONNECT_TO_CLOUD = 1;
	private static final int WAITING_RESPONSE_FROM_CLOUD = 2;
	private static final int CONNECT_TO_LOCAL_COODINATOR = 3;
	private static final int WAITING_RESPONSE_FROM_LOCAL_COORDINATOR = 4;
	private static final int WAITING_FOR_PARAMETERS = 5;
	private static final int HOLDING_TO_RUN = 6;
	private static final int RUNNING = 7;
	private static final int HOLDING_FOR_SIMULATION_INIT = 8;
	private static final int STARTING = 9;
	private static final int STOPING_CLIENT = 10;
	private static final int END = 11;

	private long simulationInitTime;
	private long start;
	private long duration;
	private int experimentCycles;
	private int executionDayCache = 1;
	private int step =  FIND_CLOUD_MANAGER;
	private boolean firstExecution = true;
	
	private String replyWithId;
	
	private Thread clockThread;
	
	public NodeStartupBehaviour() {
		
	}
	
	@Override
	public void action() {
		switch (step) {
			case FIND_CLOUD_MANAGER: 
				DFAgentDescription template = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("cloud-manager");
				template.addServices(sd);
				
				try {
					DFAgentDescription[] result = DFService.search(getAgent(), template);
					if(result.length > 0) {
						getAgent().cloudManagers = new AID[result.length];
						getAgent().cloudManagersServerInfo = new ServerInfo[result.length];
						for (int i = 0; i < result.length; ++i) {
							getAgent().cloudManagers[i] = result[i].getName();
							ServiceDescription cloudSD = (ServiceDescription) result[i].getAllServices().next();
							Properties cloudProperties = new Properties();
							cloudSD.getAllProperties().forEachRemaining(
									p-> {
										Property propertie = (Property) p;
										cloudProperties.setProperty(propertie.getName(), (String) propertie.getValue());
									}
							);
							
							ServerInfo serverInfo = new ServerInfo(result[i].getName().getName(), cloudProperties.getProperty("ip"), Integer.valueOf(cloudProperties.getProperty("port")), result[i].getName());
							getAgent().cloudManagersServerInfo[i] = serverInfo;
							
							//getAgent().addBehaviour(new CheckLinkBehaviour(serverInfo));
						}
						
						step = CONNECT_TO_CLOUD;
					} else {
						block(1000);
					}
				} catch (FIPAException e) {
					e.printStackTrace();
				}
			break;
			
			case CONNECT_TO_CLOUD:
				ACLMessage message = new ACLMessage(ACLMessage.SUBSCRIBE);
				message.setConversationId(NODE_REGISTRATION_CONVERSATION_ID);
				message.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
				
				if(getAgent().cloudManagers != null) {
					for(AID nodeAID : getAgent().cloudManagers) {
						message.addReceiver(nodeAID);
					}
				}
				
				message.setReplyWith("node-reg-"+System.currentTimeMillis());
				
				String content = getAgent().getLatitude() + ";" + getAgent().getLongitude();
				message.setContent(content);
				
				myAgent.send(message);
				
				System.out.println("Sending connection request to fog");
				getAgent().traceReport.add("Sending connection request to fog");
				
				step = WAITING_RESPONSE_FROM_CLOUD;
			break;
			
			case WAITING_RESPONSE_FROM_CLOUD:
				ACLMessage msgNodeRegistry = myAgent.receive(mtNodeRegistryMessage);
				if(msgNodeRegistry != null) {
					if(msgNodeRegistry.getPerformative() == ACLMessage.AGREE) {
						System.out.println("Received AGREE response for fog connection");
						getAgent().traceReport.add("Received AGREE response for fog connection");
						
						if(firstExecution) {
							try {
								if(msgNodeRegistry.getContentObject() != null) {
									CloudConnectResponse cloudConnectResponse = (CloudConnectResponse) msgNodeRegistry.getContentObject();
									if(cloudConnectResponse != null && cloudConnectResponse.localCoordinator != null) {
										getAgent().localCoordinator = cloudConnectResponse.localCoordinator;
										getAgent().fogServiceModel = FogServiceModel.parse(cloudConnectResponse.fogServiceModelValue);
										if(getAgent().nodeTRMApproach == null) {
											if(getAgent().fogServiceModel == FogServiceModel.PARTIALLY_OPEN || getAgent().fogServiceModel == FogServiceModel.FIXED) {
												getAgent().nodeTRMApproach = NodeTRMApproach.WR_FROM_LOCAL_COORDINATOR;
											} else {
												getAgent().nodeTRMApproach = NodeTRMApproach.CR_FROM_SUPERNODE;
											}
										}
										
										step = CONNECT_TO_LOCAL_COODINATOR;
									} else {
										getAgent().fogServiceModel = FogServiceModel.OPEN;
										if(getAgent().nodeTRMApproach == null) {
											getAgent().nodeTRMApproach = NodeTRMApproach.CR_FROM_SUPERNODE;
										}
										step = WAITING_FOR_PARAMETERS;
									}
								} else {
									getAgent().fogServiceModel = FogServiceModel.OPEN;
									if(getAgent().nodeTRMApproach == null) {
										getAgent().nodeTRMApproach = NodeTRMApproach.CR_FROM_SUPERNODE;
									}
									step = WAITING_FOR_PARAMETERS;
								}
							} catch (UnreadableException e) {
								e.printStackTrace();
							}
						} else {
							//step = STARTING;
							step = WAITING_FOR_PARAMETERS;
						}
					} else {
						System.out.println("Received REFUSE response for fog connection");
						getAgent().traceReport.add("Received REFUSE response for fog connection");
						
						step = STOPING_CLIENT;
					}
				} else {
					block();
				}
			break;
			
			case CONNECT_TO_LOCAL_COODINATOR:
				replyWithId = "lc-node-reg-"+System.currentTimeMillis();
				ACLMessage mToLocalCoodinator = new ACLMessage(ACLMessage.SUBSCRIBE);
				mToLocalCoodinator.setConversationId(NODE_REGISTRATION_CONVERSATION_ID);
				mToLocalCoodinator.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
				mToLocalCoodinator.addReceiver(getAgent().localCoordinator);
				mToLocalCoodinator.setReplyWith(replyWithId);
				
				myAgent.send(mToLocalCoodinator);
				
				System.out.println("Sending connection request to local coordinator");
				getAgent().traceReport.add("Sending connection request local coordinator");
				
				step = WAITING_RESPONSE_FROM_LOCAL_COORDINATOR;
			break;
			
			case WAITING_RESPONSE_FROM_LOCAL_COORDINATOR:
				ACLMessage msgLocalNodeRegistry = myAgent.receive(MessageTemplate.and(
						MessageTemplate.MatchInReplyTo(replyWithId),
						MessageTemplate.and(
							MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE),
							MessageTemplate.or(
								MessageTemplate.MatchPerformative(ACLMessage.AGREE),
								MessageTemplate.MatchPerformative(ACLMessage.REFUSE)
							)
						)));
				if(msgLocalNodeRegistry != null) {
					if(msgLocalNodeRegistry.getPerformative() == ACLMessage.AGREE) {
						System.out.println("Received AGREE response from local coodinator connection");
						getAgent().traceReport.add("Received AGREE response from local coodinator connection");			
						if(getAgent().nodeTRMApproach == null) {
							getAgent().nodeTRMApproach = NodeTRMApproach.WR_FROM_LOCAL_COORDINATOR;
						}
					} else {
						System.out.println("Received REFUSE response from local coodinator connection");
						getAgent().traceReport.add("Received REFUSE response from local coodinator connection");
						getAgent().localCoordinator = null;
					}
					
					step = WAITING_FOR_PARAMETERS;
				}
			break;
			
			case WAITING_FOR_PARAMETERS:
				ACLMessage msg = myAgent.receive(mtParamsMessage);
				if(msg != null) {
					String paramsStr = msg.getContent(); //params = <simulationInitTime>;<startInit>;<startEnd>;<duration>
					if(paramsStr != null && !paramsStr.isEmpty()) {
						String [] params = paramsStr.split(";");
						if(params.length >= 5) {
							simulationInitTime = Long.valueOf(params[0]);
							long startInit = Long.valueOf(params[1]);
							long startEnd = Long.valueOf(params[2]);
							duration = Long.valueOf(params[3]);
							start = ThreadLocalRandom.current().nextInt((int) startInit, (int) startEnd);
							start = new Date().getTime(); //+ (start * NodeAgent.HOUR);
							experimentCycles = Integer.valueOf(params[4]);
							getAgent().interationCount = Integer.valueOf(params[5]);
							
							System.out.println("Received params for simulation, duration: " + duration);
							getAgent().traceReport.add("Received params for simulation");
							
							if(getAgent().setupNewAgent > 0 && getAgent().interationCount == getAgent().setupNewAgent) {
								getAgent().createNewNodeAgent();
							}
							
							step = HOLDING_FOR_SIMULATION_INIT;
						} else {
							getAgent().traceReport.add("Received wrong params");
							System.out.println("ERROR - Wrong number of params for simulation - ending....");
							getAgent().doDelete();
						}
					}
				} else {
					block();
				}
			break;
			
			// TODO - Separar essa parte em um Behaviour separado
			case HOLDING_FOR_SIMULATION_INIT:
				if((new Date()).getTime() >= simulationInitTime) {
					System.out.println("Reached simulation start time - starting node execution - " + simulationInitTime);
					getAgent().traceReport.add("Reached simulation start time - starting node execution");
					step = STARTING;
				} else {
					block(500);
				}
			break;
			
			case STARTING:
				NodeAgent agent = (NodeAgent) myAgent;
				myAgent.addBehaviour(new RequestServiceBehaviour());
				//myAgent.addBehaviour(new AssessServiceBehaviour(myAgent, NodeAgent.HOUR, agent.getLossPctThreshold(), agent.getRatingHistorySize(), agent.getLatencyRequirement()));
				if(firstExecution) {
					myAgent.addBehaviour(new NodeTRMServerBehaviour());
				}
				
				getAgent().traceReport.add("Adding Behaviours and going to hold for the start: " + start);
				
				step = HOLDING_TO_RUN;
			break;
			
			case HOLDING_TO_RUN:
				if(getAgent().nodeStatus == NodeStatus.READY && start <= new Date().getTime()) {
					System.out.println("Starting to run client for" + duration);
					//getAgent().traceReport.add("Starting to run client for " + duration);
					getAgent().traceReport.add("STARTING_CLIENT_SERVICE;" + Clock.systemUTC().millis());
					
					getAgent().setupFogClient(duration);
					getAgent().startFogClient();
					getAgent().addBehaviour(new AssessServiceBehaviour(getAgent(), NodeAgent.MINUTE, getAgent().getLossPctThreshold(), getAgent().getRatingHistorySize(), getAgent().getLatencyRequirement()));
					executionDayCache = getAgent().interationCount;
					
					getAgent().startClock();
					
					step = RUNNING;
				}
			break;
			
			case RUNNING:
				if(getAgent().nodeStatus == NodeStatus.STARTED) {
					/*duration = duration - (int)((new Date().getTime()) - NodeStartupBehaviour.clock);
					System.out.println(duration);
					if(duration <= 0) {
						step = STOPING_CLIENT;
					}*/
				} else if(getAgent().nodeStatus == NodeStatus.INACTIVE){
					if(getAgent().interationCount <= experimentCycles) {
						System.out.println("Iniciando um novo dia " + getAgent().interationCount);
						getAgent().traceReport.add("ENDING_CYCLE_EXECUTION;" + Clock.systemUTC().millis());
						getAgent().collectData();
						getAgent().addBehaviour(new ShareRatingsBehaviour(getAgent().server));
						resetSimulation();
						firstExecution = true;
						step = FIND_CLOUD_MANAGER; 
					} else {
						step = STOPING_CLIENT;
					}
				}
			break;
			
			case STOPING_CLIENT:
				System.out.println("STOPING AGENT");
				getAgent().traceReport.add("STOPING AGENT");
				//getAgent().stopFogClient();
				getAgent().collectData();
				
				if(!(myAgent instanceof SupernodeAgent)) {
					getAgent().doSuspend();
				}
				step = END;
			break;
		}
	}
	
	public NodeAgent getAgent() {
		return (NodeAgent) myAgent;
	}
	
	protected void writeReport() {
		
	}
	
	private void resetSimulation() {
		
		ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
		message.setConversationId(SIMULATION_ITERATION_CONTROL_CONVERSATION);
		message.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
		
		if(getAgent().cloudManagers != null) {
			for(AID nodeAID : getAgent().cloudManagers) {
				message.addReceiver(nodeAID);
			}
		}
		
		message.setContent("end iteration");
		
		myAgent.send(message);
		
		getAgent().stopClock();
		getAgent().cloudManagers = null;
	}
	
	@Override
	public boolean done() {
		return step == END;
	}

}
