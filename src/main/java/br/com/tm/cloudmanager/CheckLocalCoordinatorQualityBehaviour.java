package br.com.tm.cloudmanager;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.repfogagent.trm.components.InteractionTrustComponent;
import br.com.tm.repfogagent.trm.components.WitnessReputationComponent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class CheckLocalCoordinatorQualityBehaviour extends Behaviour {

	private static final long serialVersionUID = 1L;
	
	private static final String GET_NODE_RATINGS_FOR_LOCAL_COORDINATOR = "get-node-ratings-for-lc";

	MessageTemplate mtLCRatingsMessage = MessageTemplate.and(
			MessageTemplate.MatchConversationId(GET_NODE_RATINGS_FOR_LOCAL_COORDINATOR),
			MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_QUERY)
	);
	
	private static final int REQUEST_NODE_RATINGS_FOR_LC = 0;
	private static final int WAIT_FOR_RATINGS_RESPONSE = 1;
	private static final int END = 2;
	
	private int step = REQUEST_NODE_RATINGS_FOR_LC;
	
	Map<String, Map<String, List<Rating>>> localCoordinatorsRatingCache;
	int countSendedQueries = 0;
	int countTrysQueries = 0;
	int countReceivedQueriesResponse;
	private String localCoordinator;
	
	public CheckLocalCoordinatorQualityBehaviour(String localCoordinator) {
		this.localCoordinator = localCoordinator;
	}
	
	@Override
	public void action() {
		switch (step) {
			case REQUEST_NODE_RATINGS_FOR_LC:
				ACLMessage message = new ACLMessage(ACLMessage.QUERY_REF);
				message.setConversationId(GET_NODE_RATINGS_FOR_LOCAL_COORDINATOR);
				message.setProtocol(FIPANames.InteractionProtocol.FIPA_QUERY);
		
				System.out.println("Sending local coordinatores ratings request");
				
				List<AID> nodes = getNodeReferralsToLocalCoordinator(this.localCoordinator, 5);
				
				if(nodes  != null) {
					for(AID nodeAID : getAgent().nodes.stream().filter(n -> !n.getName().contains("supernode")).collect(Collectors.toList())) {
						message.addReceiver(nodeAID);
						countSendedQueries++;
					}
				}
				
				countReceivedQueriesResponse = 0;
				countTrysQueries = 3;
				localCoordinatorsRatingCache = new HashMap<>();
				myAgent.send(message);
				
				step = WAIT_FOR_RATINGS_RESPONSE;
			break;
	
			case WAIT_FOR_RATINGS_RESPONSE:
				ACLMessage msgLCRatings = myAgent.receive(mtLCRatingsMessage);
				if(msgLCRatings != null) {
					try {
						if(msgLCRatings.getPerformative() == ACLMessage.INFORM_REF) {
							List<Rating> ratings = (ArrayList<Rating>) msgLCRatings.getContentObject();
							if(ratings != null && !ratings.isEmpty()) {
								localCoordinatorsRatingCache.putIfAbsent(ratings.get(0).getServerName(), new HashMap());
								localCoordinatorsRatingCache.get(ratings.get(0).getServerName()).put(msgLCRatings.getSender().getName(), ratings);
							}
							countReceivedQueriesResponse++;
							if(countSendedQueries == countReceivedQueriesResponse) {
								decideToChangeLocalCoordinator(generateLocalCoordinatorsCalculatedRatingBase());
							}
							
							System.out.println("Received ratings for local coordinator from " + msgLCRatings.getSender().getLocalName() + ";");
							ratings.forEach(r -> System.out.print(";" + r.getValue()));
						}
					} catch (UnreadableException e) {
						e.printStackTrace();
					}
				} else {
					countTrysQueries--;
					if(countTrysQueries == 0 && countReceivedQueriesResponse >= (0.7 * (countSendedQueries))) {
						decideToChangeLocalCoordinator(generateLocalCoordinatorsCalculatedRatingBase());
					} else {
						step = END;
					}
					block(1000);
				}
			break;
		}
	}
	
	private List<AID> getNodeReferralsToLocalCoordinator(String localCoordinatorName, int numReferrals) {
		List<String> nodes = getAgent().localCoordinatorDatabase.getNodesPerLocalCoordinator(localCoordinatorName);
		if(nodes.size() > numReferrals) {
			nodes = nodes.subList(0, numReferrals);
		}
		
		return nodes.stream().map(name -> getAgent().nodes.stream().filter(n -> n.getName().equals(name)).findFirst().get()).collect(Collectors.toList());
	}
	
	private void decideToChangeLocalCoordinator(Map<String, Double[]> ratingBase) {
		System.out.println("Deciding if local coordinator needs change");
		for(Entry<String, Double[]> entry : ratingBase.entrySet()) {
			/*if(entry.getValue()[0] < getAgent().localCoordinatorQoS && entry.getValue()[1] > 0.8) {
				getAgent().addBehaviour(new ChangeLocalCoordinatorBehaviour(entry.getKey()));
			}*/
			System.out.println("{" + entry.getValue()[0] + ";" + entry.getValue()[1] + "}");
			
			getAgent().addBehaviour(new ChangeLocalCoordinatorBehaviour(getAgent().localCoordinatorDatabase.getLocalCoordinator(entry.getKey())));
		}
		
		step = END;
	}
	
	private Map<String, Double[]> generateLocalCoordinatorsCalculatedRatingBase() {
		Map<String, Double[]> localCoordinatorsCalculatedRating = new HashMap<>();
		
		if(localCoordinatorsRatingCache != null && !localCoordinatorsRatingCache.isEmpty()) {
			
			//Runs for each local coordinator
			for(Entry<String, Map<String, List<Rating>>> lcRatingCache : localCoordinatorsRatingCache.entrySet()) {
				
				Map<String, Rating> ratingsPerNode = new HashMap<>();
				lcRatingCache.getValue().entrySet().forEach(e -> {
					ratingsPerNode.put(e.getKey(), e.getValue().stream().filter(r -> r.getTerm().equals("availability")).findFirst().get());
				});
				
				WitnessReputationComponent witnessReputationComponent = new WitnessReputationComponent(0, 1.0, 1.0);
				Map<Integer, Map<String, Double>> witnessCredibilityBase = new HashMap<>();
				
				Map<String, Double> customWitnessCredibility = witnessReputationComponent.customWitnessCredibility(ratingsPerNode);
				
				witnessCredibilityBase.put(0, customWitnessCredibility);
				
				
				Map<String, List<Rating>> ratingsForWitness = new HashMap<>();
				for(Entry<Integer, Map<String, Double>> witnessCredibility : witnessCredibilityBase.entrySet()) {
					for(Entry<String, Double> entry : witnessCredibility.getValue().entrySet()) {
						Rating rating = new Rating("cloud", entry.getKey(), entry.getValue(), entry.getValue(), witnessCredibility.getKey(), "termWRLC", new Date());
						ratingsForWitness.putIfAbsent(entry.getKey(), new ArrayList<>());
						ratingsForWitness.get(entry.getKey()).add(rating);
					}
				}
				
				InteractionTrustComponent interactionTrustComponent = new InteractionTrustComponent(0, 1.0);
				double sumWeightValue = 0;
				double sumValues = 0;
				double reliabilityIT = 0;
				for(Entry<String, List<Rating>> entry : ratingsForWitness.entrySet()) {
					double interactionTrustValue = interactionTrustComponent.calculate(entry.getValue(), getAgent().iteration);
					reliabilityIT = interactionTrustComponent.reliability(entry.getValue());
					sumValues += ratingsPerNode.get(entry.getKey()).getValue() * interactionTrustValue;
					sumWeightValue += interactionTrustValue;
				}
				
				localCoordinatorsCalculatedRating.put(lcRatingCache.getKey(), new Double [] {(sumValues / sumWeightValue), reliabilityIT});
			}
		}
		
		return localCoordinatorsCalculatedRating;
	}
	
	@Override
	public boolean done() {
		return step == END;
	}

	@Override
	public CloudManagerAgent getAgent() {
		return (CloudManagerAgent) myAgent;
	}
	
}
