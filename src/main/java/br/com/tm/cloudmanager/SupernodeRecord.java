package br.com.tm.cloudmanager;

import br.com.tm.util.Geolocation;
import jade.core.AID;

public class SupernodeRecord {
	public AID supernodeAID;
	public Geolocation geolocation;
	public int capacity;
	public int port;
	public String ip;
	
	public SupernodeRecord(AID supernodeAID, Geolocation geolocation, int capacity, int port, String ip) {
		super();
		this.supernodeAID = supernodeAID;
		this.geolocation = geolocation;
		this.capacity = capacity;
		this.port = port;
		this.ip = ip;
	}
}