package br.com.tm.cloudmanager;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.report.Report;
import br.com.tm.report.ReportFactory;
import br.com.tm.server.UDPServer;
import br.com.tm.util.Configuration;
import br.com.tm.util.FogServiceModel;
import br.com.tm.util.SimulatorGen;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.Property;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class CloudManagerAgent extends Agent {

	private static final long serialVersionUID = 1L;
	
	private static final int BASE_BANDWIDTH_FOR_CLOUD = 5376000; //(in CPS)
	
	protected List<AID> supportedNodes;

	protected List<AID> nodes;
	protected SupernodesDatabase supernodesDatabase;
	protected LocalCoordinatorDatabase localCoordinatorDatabase;
	protected Map<String, List<Rating>> generalRatingDataBase;
	
	protected UDPServer udpServer;

	/**
	 * Parameters for simulation
	 */
	protected int numNodes;
	protected int numSupernodes;
	protected int numClouds;
	protected int numSupernodesToFail;
	protected int numLocalCoordinatorsToFail;
	protected long[][] simulationNodeStartDurationDistribution;
	protected double[][] supernodesFailurePct;
	protected int[][] localCoordinatorsFailurePct;
	protected int indexNodeForSimulationParams;
	protected boolean simulationStarted = false; 
	protected long simulationStartTime;
	protected int experimentCycles;
	protected int iteration = 1;
	protected int numIterationResponses;
	/***********************************/
	
	protected double localCoordinatorQoS = 0.8;
	protected int minimunLatencyThreshold;
	protected Report report;
	protected Report simulationReport;
	protected double generalTrustValue;
	protected int iterationCheckPointPatter = 4;
	
	protected int cloudServerPort = 9876;
	protected String cloudServerHostIP;
	
	protected double latitude;
	protected double longitude;
	
	protected FogServiceModel fogServiceModel = FogServiceModel.PARTIALLY_OPEN;
	protected boolean fullyControlSimulation = true;
	protected Map<String, Integer> iterationMapCacheControl;
	protected RatingDatabase ratingDatabase;
	
	@Override
	protected void setup() {
		super.setup();
		System.out.println("Hello i'm a CloudManager");
		
		supernodesDatabase = new SupernodesDatabase(Configuration.MINIMUN_LATENCY_THRESHOLD);
		localCoordinatorDatabase = new LocalCoordinatorDatabase(Configuration.LOCAL_COORDINATOR_DISTANCE_LIMIT);
		supportedNodes = new ArrayList<>();
		nodes = new ArrayList<>();
		generalRatingDataBase = new HashMap<>();
		iterationMapCacheControl = new HashMap<>();
		
		report = ReportFactory.getReport(this);
		simulationReport = ReportFactory.getReport("simulation_report");
		
		getParametersFromArguments("numNodes", "numSupernodes", "numClouds", "experimentCycles", "numSupernodesToFail", "minimunLatencyThreshold", "numLocalCoordinatorsToFail");
		
		try {
			cloudServerHostIP = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		
		try {
			registerAgentService();
		} catch (UnknownHostException e) {
			System.exit(1);
		}
		
		initCloudFogService();
		
		loadSupernodeFailure();
		
		loadLocalCoordinatorFailure();
		
		addBehaviour(new CloudFogServiceBehaviour());
		addBehaviour(new SupernodesSearchServerBehaviour());
		addBehaviour(new NodeRegistrationServiceBehaviour());
		addBehaviour(new LocalCoordinatorRegistrationServiceBehaviour());
		addBehaviour(new SupernodeRegistrationServiceBehaviour(false));
		addBehaviour(new SimulationControlBehaviour());
	}
	
	protected void registerAgentService() throws UnknownHostException {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("cloud-manager");
		sd.setName("Fog-Cloud-Manager");
		sd.addProperties(new Property("ip", cloudServerHostIP));
		sd.addProperties(new Property("port", cloudServerPort));
		sd.addProperties(new Property("latitude", latitude));
		sd.addProperties(new Property("longitude", longitude));
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}
	
	private void initCloudFogService() {
		udpServer = new UDPServer(9876, BASE_BANDWIDTH_FOR_CLOUD);
		udpServer.run();
	}
	
	protected void getParametersFromArguments(String ... paramatersName) {
		if(getArguments() != null && getArguments().length > 0) {
			for(String parameter : paramatersName) {
				int i = 0;
				while(i < getArguments().length) {
					Object a = getArguments()[i]; 
					if(a instanceof String) {
						if(((String) a).equalsIgnoreCase(parameter)) {
							try {
								if(i + 1 < getArguments().length) {
									if(Arrays.stream(this.getClass().getDeclaredFields()).filter(f -> f.getName().equals(parameter)).count() > 0) {
										this.getClass().getDeclaredField(parameter).set(this, dynamicTypeSetting(this.getClass().getDeclaredField(parameter).getType(), getArguments()[i + 1].toString()));
									} else {
										System.out.println(this.getClass().getSuperclass().getName());
										this.getClass().getSuperclass().getDeclaredField(parameter).set(this, dynamicTypeSetting(this.getClass().getDeclaredField(parameter).getType(), getArguments()[i + 1].toString()));
									}
									
									i+=2;
									break;
								}
							} catch (NumberFormatException e) {
								throw new RuntimeException("Invalid arguments for capacity parameter");
							} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
								e.printStackTrace();
								throw new RuntimeException("Invalid arguments for capacity parameter");
							}
						}
					}
					i++;
				}
			}
		}
	}
	
	protected Object dynamicTypeSetting(Class fieldType, String value) {
		if(fieldType.getName().equals(Integer.class.getName()) || fieldType.getName().equals("int")) {
			return Integer.valueOf(value);
		} else if(fieldType.getName().equals(Long.class.getName()) || fieldType.getName().equals("long")) {
			return Long.valueOf(value);
		} else if(fieldType.getName().equals(Double.class.getName()) || fieldType.getName().equals("double")) {
			return Double.valueOf(value);
		}
		
		return null;
	}
	
	protected void loadSimulationParams() {
		indexNodeForSimulationParams = 0;
		simulationNodeStartDurationDistribution = SimulatorGen.generateNodesStartEndTime(100);
	}
	
	protected void loadSupernodeFailure() {
		if(numSupernodesToFail > 0) {
			supernodesFailurePct = new double [numSupernodesToFail][1];
			for(int i = 0; i < numSupernodesToFail; i++) {
				supernodesFailurePct[i][0] = ThreadLocalRandom.current().nextDouble(0.1, 0.3);
			}
		}
	}
	
	protected void loadLocalCoordinatorFailure() {
		if(numLocalCoordinatorsToFail > 0) {
			localCoordinatorsFailurePct = new int[numLocalCoordinatorsToFail][1];
			for(int i = 0; i < numLocalCoordinatorsToFail; i++) {
				localCoordinatorsFailurePct[i][0] = ThreadLocalRandom.current().nextInt(5, 7);
			}
		}
	}
	
	protected long[] getParamsForNode() {
		return simulationNodeStartDurationDistribution[indexNodeForSimulationParams++];
	}
	
	public SupernodesDatabase getSupernodesDatabase() {
		return supernodesDatabase;
	}

	public void setSupernodesDatabase(SupernodesDatabase supernodesDatabase) {
		this.supernodesDatabase = supernodesDatabase;
	}
	
	public List<AID> getNodes() {
		return nodes;
	}

	protected double getSupernodeFailurePct() {
		if(numSupernodesToFail > 0 && supernodesFailurePct != null) {
			for(int i = 0; i < supernodesFailurePct.length; i++) {
				if(supernodesFailurePct[i][0] > 0.0) {
					return supernodesFailurePct[i][0]; 
				}
			}
		}
		
		return 0.0;
	}
	
	protected int getLocalCoordinatorFailurePct() {
		if(numLocalCoordinatorsToFail > 0 && localCoordinatorsFailurePct != null) {
			for(int i = 0; i < localCoordinatorsFailurePct.length; i++) {
				if(localCoordinatorsFailurePct[i][0] > 0.0) {
					return localCoordinatorsFailurePct[i][0]; 
				}
			}
		}
		
		return 0;
	}

	public RatingDatabase getRatingDatabase() {
		return ratingDatabase;
	}

	@Override
	public void doDelete() {
		udpServer.stop();
		report.close();
		super.doDelete();
	}
	
}
