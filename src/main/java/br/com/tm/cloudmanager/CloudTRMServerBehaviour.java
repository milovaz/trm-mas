package br.com.tm.cloudmanager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.util.Util;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class CloudTRMServerBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;

	private static final String GET_GENERAL_TRUST_CONVERSATION = "get-general-trust";
	private static final String RATING_TO_SUPERNODE_ID = "rating-to-supernode";
	
	MessageTemplate mtTrm = MessageTemplate.and(
		MessageTemplate.and(	
			MessageTemplate.or(
					MessageTemplate.MatchConversationId(GET_GENERAL_TRUST_CONVERSATION), 
					MessageTemplate.MatchConversationId(RATING_TO_SUPERNODE_ID)),
			MessageTemplate.or(
					MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
					MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_QUERY))
		),
		MessageTemplate.or(
				MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
				MessageTemplate.MatchPerformative(ACLMessage.QUERY_REF)
		)
	);
	
	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mtTrm);
		if(msg != null) {
			try {
				System.out.println("Received message from " + msg.getSender().getName());
				ACLMessage reply = msg.createReply();
				reply.setPerformative(ACLMessage.INFORM);
				
				String nodeId = msg.getSender().getName();
				
				if(msg.getConversationId().contains(RATING_TO_SUPERNODE_ID)) {
					Map<String, List<Rating>> ratings = (Map<String, List<Rating>>)msg.getContentObject();
					ratings.entrySet().forEach(e -> {
						getAgent().generalRatingDataBase.putIfAbsent(e.getKey(), new ArrayList<>());
						getAgent().generalRatingDataBase.get(e.getKey()).addAll(e.getValue());
					});
					reply.setContent("OK");
				} else if(msg.getConversationId().contains(GET_GENERAL_TRUST_CONVERSATION)) {
					reply.setContent(String.valueOf(getAgent().generalTrustValue));
				}
				myAgent.send(reply);
			} catch (UnreadableException e) {
				e.printStackTrace();
			} 
		} else {
			block();
		}
	}
	
	private void calculateGeneralTrustValue() {
		
	}

	@Override
	public CloudManagerAgent getAgent() {
		return (CloudManagerAgent) myAgent;
	}
	
}
