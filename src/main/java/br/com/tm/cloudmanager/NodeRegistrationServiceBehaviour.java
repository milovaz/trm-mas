package br.com.tm.cloudmanager;

import java.io.IOException;
import java.util.ArrayList;

import br.com.tm.util.CloudConnectResponse;
import br.com.tm.util.FogServiceModel;
import br.com.tm.util.Geolocation;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class NodeRegistrationServiceBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;
	
	private static final String NODE_REGISTRATION_CONVERSATION_ID = "node-registration-conversation-id";
	
	MessageTemplate mt = MessageTemplate.and(
			MessageTemplate.MatchConversationId(NODE_REGISTRATION_CONVERSATION_ID),
			MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE)
	);

	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mt);
		if(msg != null) {
			ACLMessage reply = msg.createReply();
			reply.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
			switch (msg.getPerformative()) {
				case ACLMessage.SUBSCRIBE:
					if(getAgent().isAlive()) {
						reply.setPerformative(ACLMessage.AGREE);
						if(!getAgent().nodes.contains(msg.getSender())) {
							getAgent().nodes.add(msg.getSender());
							String [] parameters = msg.getContent() != null && msg.getContent().length() > 0 ? msg.getContent().split(";") : null;
							try {
								if(parameters != null && parameters.length > 1) {
									Geolocation geolocation = Geolocation.parse(parameters[0], parameters[1]);
									AID localCoordinator = getAgent().localCoordinatorDatabase.searchForNearestLocalCoordinator(geolocation);
									if(localCoordinator == null) {
										reply.setContentObject(null);
										System.out.println("Agreeing to " + msg.getSender().getLocalName() +" registry - No local coordinator");
									} else {
										CloudConnectResponse cloudConnectResponse = new CloudConnectResponse(getAgent().fogServiceModel.value, localCoordinator);
										getAgent().localCoordinatorDatabase.addNodeToLocalCoordinator(localCoordinator.getName(), msg.getSender().getName());
										reply.setContentObject(cloudConnectResponse);
										
										System.out.println("Agreeing to " + msg.getSender().getLocalName() +" registry - Sending " + localCoordinator.getLocalName());
									}
									
									if(getAgent().simulationStarted) {
										getAgent().numIterationResponses++;
									}
								} else {
									reply.setContentObject(null);
								}
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					} else {
						reply.setPerformative(ACLMessage.REFUSE);
						System.out.println("Refusing node registry");
					}
				break;
	
				case ACLMessage.CANCEL:
					if(getAgent().nodes != null && getAgent().nodes.contains(msg.getSender())) {
						getAgent().nodes.remove(msg.getSender());
					}
					
					getAgent().supernodesDatabase.removeSupernode(msg.getSender());
				break;
			}
			
			myAgent.send(reply);
		} else {
			block();
		}
	}
	
	public CloudManagerAgent getAgent() {
		return (CloudManagerAgent) myAgent;
	}

}
