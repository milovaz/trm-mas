package br.com.tm.cloudmanager;

import java.util.Date;

import br.com.tm.util.Configuration;
import br.com.tm.util.SimulatorGen;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;

public class SendSimulationParamsBehaviour extends OneShotBehaviour {

	private static final long serialVersionUID = 1L;
	
	private static final String SIMULATION_PARAMAS_CONVERSATION_ID = "simulation-params-conversation-id";
	
	private AID destAID;
	
	public SendSimulationParamsBehaviour(AID destAID) {
		this.destAID = destAID;
	}
	
	@Override
	public void action() {
		ACLMessage requestServiceMessage;
		
		long simulationInitTime = new Date().getTime() + Configuration.SIMULATION_INIT_DELAY; //Data atual + 5 minutos
		
		requestServiceMessage = new ACLMessage(ACLMessage.PROXY);
		requestServiceMessage.setConversationId(SIMULATION_PARAMAS_CONVERSATION_ID);
		requestServiceMessage.setProtocol(FIPANames.InteractionProtocol.FIPA_RECRUITING);
		requestServiceMessage.addReceiver(destAID);
		
		long [] simulationParams = getAgent().getParamsForNode();
		simulationParams[2] = 2 * Configuration.HOUR;
		
		System.out.println(2 * Configuration.HOUR);
		
		//Simulation init time ; start init; end init; duration 
		String params =  simulationInitTime + ";" + simulationParams[0] + ";" + simulationParams[1] + ";" + simulationParams[2];
		requestServiceMessage.setContent(params);
		
		myAgent.send(requestServiceMessage);
		
	}
	
	public CloudManagerAgent getAgent() {
		return (CloudManagerAgent) myAgent;
	}
}
