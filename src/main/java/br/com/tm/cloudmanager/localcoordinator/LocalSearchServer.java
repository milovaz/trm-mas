package br.com.tm.cloudmanager.localcoordinator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import br.com.tm.cloudmanager.CloudManagerAgent;
import br.com.tm.util.Geolocation;
import br.com.tm.util.ServerInfo;
import br.com.tm.util.SupernodeSearchResponse;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class LocalSearchServer extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;

	MessageTemplate mt = MessageTemplate.and(
			MessageTemplate.MatchConversationId("supernodes-search"),
			MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST)
	);
	
	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mt);
		if(msg != null) {
			try {
				System.out.println("Received message from " + msg.getSender().getLocalName());
				String [] parameters = msg.getContent() != null && msg.getContent().length() > 0 ? msg.getContent().split(";") : null;
				ACLMessage reply = msg.createReply();
				
				if(!fail()) {
					reply.setPerformative(ACLMessage.INFORM);
					
					if(parameters != null && parameters.length > 1) {
						Geolocation geolocation = Geolocation.parse(parameters[0], parameters[1]);
						reply.setContentObject(selectSupernodesCandidates(msg.getSender(), geolocation));
					} else {
						reply.setContentObject(null);
					}
				} else {
					System.out.println("Local Coordinator failure!");
					reply.setPerformative(ACLMessage.FAILURE);
				}
				myAgent.send(reply);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			block();
		}
	}
	
	private boolean fail() {
		LocalCoordinatorAgent lc = (LocalCoordinatorAgent) myAgent;
		return lc.failurePct > 0 && ThreadLocalRandom.current().nextInt(10) < lc.failurePct;
	}

	private SupernodeSearchResponse selectSupernodesCandidates(AID nodeAID, Geolocation geolocation) {		
		ArrayList<ServerInfo> supernodesInfoList;
		
		List<ServerInfo> supernodes = ((CloudManagerAgent) myAgent).getSupernodesDatabase().searchForSupernodes(geolocation).stream().filter(aid -> !aid.supernodeAID.equals(nodeAID)).map(aid -> new ServerInfo(aid.supernodeAID.getName(), aid.ip, aid.port, aid.supernodeAID, aid.geolocation)).collect(Collectors.toList());
		
		SupernodeSearchResponse supernodeSearchResponse = new SupernodeSearchResponse();
		supernodeSearchResponse.supernodes = supernodes;
		
		if(((CloudManagerAgent) myAgent).getRatingDatabase() != null) {
			ArrayList<AID> referrals = new ArrayList<>();
			for(ServerInfo serverInfo : supernodes) {
				referrals.addAll(((CloudManagerAgent) myAgent).getRatingDatabase()
															  .recoverReferralsForServer(serverInfo.getSever())
															  .stream().filter(s -> !s.getName().equals(nodeAID.getName()))
															  .collect(Collectors.toList()));
			}
			
			supernodeSearchResponse.referrals = new ArrayList<>(distinctAID(referrals.stream().collect(Collectors.toList())));
			supernodeSearchResponse.referrals.forEach(r -> System.out.println(r.getName()));
		}
		
		System.out.println("Cloud Manager found " + supernodes.size() + " supernodes");
		
		if(supernodeSearchResponse.referrals != null) {
			supernodeSearchResponse.referrals.forEach(s -> s.getName());
		}
		
		return supernodeSearchResponse;
	}
	
	private Collection<AID> distinctAID(List<AID> list) {
		Map<String, AID> mapaAux = new HashMap<>();
		for(AID aid : list) {
			mapaAux.putIfAbsent(aid.getName(), aid);
		}
		return mapaAux.values();
	}
}
