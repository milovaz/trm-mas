package br.com.tm.cloudmanager.localcoordinator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.repfogagent.util.TRMHelper;
import br.com.tm.util.Geolocation;
import br.com.tm.util.Util;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class LocalTRMServerBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;
	private TRMHelper trmHelper;

	MessageTemplate mt = MessageTemplate.and(
		MessageTemplate.or(
			MessageTemplate.or(
				MessageTemplate.MatchConversationId("supernode-rating"), 
				MessageTemplate.MatchConversationId("supernode-certificate")
			),
			MessageTemplate.or(
				MessageTemplate.MatchConversationId("get-supernode-referals"),
				MessageTemplate.MatchConversationId("inform-node-add-to-supernode")
			)
		),
		MessageTemplate.or(
			MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
			MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_QUERY)
		)
	);
	
	public LocalTRMServerBehaviour() {
		this.trmHelper = TRMHelper.getInstance();
	}
	
	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mt);
		if(msg != null) {
			try {
				System.out.println("Received message from " + msg.getSender().getLocalName());
				System.out.println(msg.getConversationId());
				ACLMessage reply = msg.createReply();
				reply.setPerformative(ACLMessage.INFORM);
				
				String nodeId = msg.getSender().getName();
				
				if(msg.getConversationId().contains("supernode-rating")) {
					Map<String, List<Rating>> ratings = (Map<String, List<Rating>>)msg.getContentObject();
					//ratings.entrySet().forEach(e -> getAgent().localRatingDatabase.insert(nodeId, e.getKey(), e.getValue()));
					reply.setContent("OK");
				} else if(msg.getConversationId().contains("supernode-certificate")) {
					String query = msg.getContent();
					String [] serversIds = getServersIdFromQuery(query);
					if(query.contains("calculated-certificate")) {
						HashMap<String, Double[]> certificates = new HashMap<>();
						for(String serverId : serversIds) {
							Double [] certificate = trmHelper.calcSuperNodesCertificate(nodeId, serverId, getAgent().getRatingDatabase().recover(nodeId, serverId), getAgent().getRatingDatabase().recoverForServer(serverId), Util.current(), 1);
							certificates.put(serverId, certificate);
						}
						reply.setContentObject(certificates);
					} else if(query.contains("list-ratings")) {
						ArrayList<Map<String, List<Rating>>> listResp = new ArrayList<>();
						for(String serverId : serversIds) {
							listResp.add(getAgent().getRatingDatabase().recoverForServer(serverId));
						}
						reply.setContentObject(listResp);
					}
				} else if(msg.getConversationId().contains("get-supernode-referals")) {
					System.out.println("Searching for supernode referals");
					HashMap<String, List<String>> referals = new HashMap<>();
					String serverIdList = msg.getContent();
					String [] serversIds = serverIdList.split("#;#");
					ArrayList<AID> referrals = new ArrayList<>();
					for(String serverId : serversIds) {
						referrals.addAll(getAgent().getRatingDatabase().recoverReferralsForServer(serverId));
					}
					
					reply.setContentObject(new ArrayList(referrals.stream().distinct().collect(Collectors.toList())));
				} else if(msg.getConversationId().contains("inform-node-add-to-supernode")) {
					AID node = (AID) msg.getContentObject();
					if(node != null) {
						getAgent().getRatingDatabase().insert(node, msg.getSender(), null);
					}
					reply.setContent("OK");
				}
				myAgent.send(reply);
			} catch (UnreadableException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			block();
		}
	}
	
	private String[] getServersIdFromQuery(String query) {
		String [] queryParts = query.split(":");
		return queryParts[1].split("#;#");
	}
	
	@Override
	public LocalCoordinatorAgent getAgent() {
		return (LocalCoordinatorAgent) myAgent;
	}

}
