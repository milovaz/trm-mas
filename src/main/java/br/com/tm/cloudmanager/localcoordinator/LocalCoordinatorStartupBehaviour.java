package br.com.tm.cloudmanager.localcoordinator;

import br.com.tm.util.Configuration;
import br.com.tm.util.FogServiceModel;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class LocalCoordinatorStartupBehaviour extends Behaviour {

	private static final long serialVersionUID = 1L;
	
	private static final String LOCAL_COORDINATOR_REGISTRATION_CONVERSATION_ID = "local-coordinator-registration-conversation-id";
	private static final String SIMULATION_PARAMAS_CONVERSATION_ID = "simulation-params-conversation-id";
	
	MessageTemplate mtLocalCoordRegistryMessage = MessageTemplate.and(
			MessageTemplate.MatchConversationId(LOCAL_COORDINATOR_REGISTRATION_CONVERSATION_ID),
			MessageTemplate.and(
					MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE),
					MessageTemplate.or(
						MessageTemplate.MatchPerformative(ACLMessage.AGREE),
						MessageTemplate.MatchPerformative(ACLMessage.REFUSE)
					)
				)
	);
	
	MessageTemplate mtParamsMessage = MessageTemplate.and(
			MessageTemplate.MatchConversationId(SIMULATION_PARAMAS_CONVERSATION_ID),
			MessageTemplate.and(
					MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE),
					MessageTemplate.MatchPerformative(ACLMessage.INFORM)
				)
	);
	
	
	
	private static final int FIND_CLOUD_MANAGER = 0;
	private static final int CONNECT_TO_CLOUD = 1;
	private static final int WAITING_RESPONSE_FROM_CLOUD = 2;
	private static final int WAITING_FOR_PARAMETERS = 3;
	private static final int STOPING_LOCAL_COORDINATOR = 4;
	private static final int END = 5;
	
	private int step = FIND_CLOUD_MANAGER;
	
	@Override
	public void action() {
		switch (step) {
			case FIND_CLOUD_MANAGER: 
				DFAgentDescription template = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("cloud-manager");
				template.addServices(sd);
				
				try {
					DFAgentDescription[] result = DFService.search(getAgent(), template);
					if(result.length > 0) {
						getAgent().cloudManagers = new AID[result.length];
						for (int i = 0; i < result.length; ++i) {
							getAgent().cloudManagers[i] = result[i].getName();
						}
						step = CONNECT_TO_CLOUD;
					} else {
						block(1000);
					}
				} catch (FIPAException e) {
					e.printStackTrace();
				}
			break;
			
			case CONNECT_TO_CLOUD:
				ACLMessage message = new ACLMessage(ACLMessage.SUBSCRIBE);
				message.setConversationId(LOCAL_COORDINATOR_REGISTRATION_CONVERSATION_ID);
				message.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
				
				if(getAgent().cloudManagers != null) {
					for(AID nodeAID : getAgent().cloudManagers) {
						message.addReceiver(nodeAID);
					}
				}
				
				message.setReplyWith("node-reg-"+System.currentTimeMillis());
				
				String content = 100 + ";" + getAgent().latitude + ";" + getAgent().longitude;
				message.setContent(content);
				
				myAgent.send(message);
				
				System.out.println("Sending connection request to fog");
				
				step = WAITING_RESPONSE_FROM_CLOUD;
			break;
			
			case WAITING_RESPONSE_FROM_CLOUD:
				ACLMessage msgNodeRegistry = myAgent.receive(mtLocalCoordRegistryMessage);
				if(msgNodeRegistry != null) {
					if(msgNodeRegistry.getPerformative() == ACLMessage.AGREE) {
						System.out.println("Received AGREE response for fog connection");
						
						step = WAITING_FOR_PARAMETERS;
					} else {
						System.out.println("Received REFUSE response for fog connection");
											
						step = STOPING_LOCAL_COORDINATOR;
					}
				} else {
					block();
				}
			break;
			
			case WAITING_FOR_PARAMETERS:
				ACLMessage msgSimulationParams = myAgent.receive(mtParamsMessage);
				if(msgSimulationParams != null) {
					String paramsStr = msgSimulationParams.getContent();
					if(paramsStr != null && !paramsStr.isEmpty()) {
						String [] params = paramsStr.split(";");
						getAgent().failurePct = Double.valueOf(params[0]).intValue();
						getAgent().totalNodes = Double.valueOf(params[1]).intValue();
						getAgent().iteration = Integer.valueOf(params[2]).intValue();
						
						System.out.println("Setting failure percentage to " + getAgent().failurePct);
						System.out.println("Iteration " + getAgent().iteration);
						
						if((getAgent().iteration % getAgent().localTrustHistorySize) == 0) {
							System.out.println("Adding LocalFogControlBehaviour");
							myAgent.addBehaviour(new LocalFogControlBehaviour());
						}
					}
				} else {
					block();
				}
			break;
			
			case STOPING_LOCAL_COORDINATOR:
				getAgent().doSuspend();
				step = END;
			break;
		}
	}

	@Override
	public boolean done() {
		return step == END;
	}

	@Override
	public LocalCoordinatorAgent getAgent() {
		return (LocalCoordinatorAgent) myAgent;
	}
	
}
