package br.com.tm.cloudmanager.localcoordinator;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import br.com.tm.repfogagent.util.StatisticUtil;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

public class UpdateLocalTrustBehaviour extends AchieveREInitiator {

	private static final long serialVersionUID = 1L;

	List<Double> deviations = new ArrayList<>();
	
	public UpdateLocalTrustBehaviour(Agent a, ACLMessage msg) {
		super(a, msg);
	}

	@Override
	protected void handleAllResultNotifications(Vector resultNotifications) {
		Enumeration e = resultNotifications.elements();
		while(e.hasMoreElements()) {
			ACLMessage msg = (ACLMessage) e.nextElement();
			if(msg.getPerformative() == ACLMessage.INFORM) {
				if(msg.getContent() != null) {
					deviations.add(Double.valueOf(msg.getContent()));
				}
			}
		}
		
		System.out.println("Calculating local trust");
		
		calculateLocalTrust();
		
		System.out.println("Local Trust calculated - " + getAgent().localTrust);
	}
	
	private void calculateLocalTrust() {
		double localTrust = 1;
		
		double variation = StatisticUtil.coefficientVariationRaw(deviations);
		
		getAgent().localTrust = localTrust - variation;
	}
	
	@Override
	public LocalCoordinatorAgent getAgent() {
		return (LocalCoordinatorAgent) myAgent;
	}

}
