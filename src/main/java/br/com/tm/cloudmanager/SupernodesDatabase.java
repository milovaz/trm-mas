package br.com.tm.cloudmanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import br.com.tm.util.Geolocation;
import br.com.tm.util.ServerInfo;
import br.com.tm.util.Util;
import jade.core.AID;

public class SupernodesDatabase {

	private Map<String, SupernodeRecord> supernodes;
	private int minimunLatencyThreshold;
	
	public SupernodesDatabase(int minimunLatencyThreshold) {
		this.minimunLatencyThreshold = minimunLatencyThreshold;
		this.supernodes = new HashMap<>();
	}
	
	public void addSupernode(AID supernodeAID, Geolocation geolocation, int capacity, int port, String ip) {
		supernodes.putIfAbsent(supernodeAID.getName(), new SupernodeRecord(supernodeAID, geolocation, capacity, port, ip));
	}
	
	public void removeSupernode(AID supernodeAID) {
		supernodes.remove(supernodeAID.getName());
	}
	
	public List<SupernodeRecord> getSupernodes() {
		return new ArrayList<SupernodeRecord>(supernodes.values());
	}

	public List<SupernodeRecord> searchForSupernodes(Geolocation requesterGeolocation) {
		if(supernodes == null ||  supernodes.isEmpty()) {
			return new ArrayList<>();
		}
		
		if(requesterGeolocation != null) {
			return searchForNearbySupernodes(requesterGeolocation.getLatitude(), requesterGeolocation.getLongitude());
		} else {
			return supernodes.values().stream().collect(Collectors.toList());
		}
	}

	public int count() {
		return supernodes.size();
	}
	
	private List<SupernodeRecord> searchForNearbySupernodes(double lat, double lon) {
		Util util = new Util();
		return supernodes.entrySet().stream().filter((e) -> {
			int latency = kmToLatencyRatio(distance(e.getValue().geolocation.getLatitude(), 
						  									  e.getValue().geolocation.getLongitude(), 
						  									  lat, lon, 'K'));
			return latency <= this.minimunLatencyThreshold;	
		}).map(e -> e.getValue())
		.collect(Collectors.toList());
	}

	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  Geolocation functions                                         :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	public double distance(Geolocation geolocationA, Geolocation geolocationB) {
		return distance(geolocationA.getLatitude(), geolocationA.getLongitude(), geolocationB.getLatitude(), geolocationB.getLongitude(), 'K');
	}
	
	public double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == 'K') {
			dist = dist * 1.609344;
		} else if (unit == 'N') {
			dist = dist * 0.8684;
		}
		return dist;
	}
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts decimal degrees to radians             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts radians to decimal degrees             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}
	
	public int kmToLatencyRatio(double distInKm) { 
		return (int)Math.round(1000*((distInKm * 1000) / 1.85e8));  
	}
	
	public double latencyToKmRatio(int latencyInMs) {
		return (1.85e8 * latencyInMs) / (1000000);
	}
	
}
