package br.com.tm.cloudmanager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import br.com.tm.util.Geolocation;
import br.com.tm.util.ServerInfo;
import br.com.tm.util.SupernodeSearchResponse;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class SupernodesSearchServerBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;

	MessageTemplate mt = MessageTemplate.and(
			MessageTemplate.MatchConversationId("supernodes-search"),
			MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST)
	);
	
	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mt);
		if(msg != null) {
			try {
				System.out.println("Received message from " + msg.getSender().getLocalName());
				String [] parameters = msg.getContent() != null && msg.getContent().length() > 0 ? msg.getContent().split(";") : null;
				ACLMessage reply = msg.createReply();
				reply.setPerformative(ACLMessage.INFORM);
				
				if(parameters != null && parameters.length > 1) {
					Geolocation geolocation = Geolocation.parse(parameters[0], parameters[1]);
					reply.setContentObject(selectSupernodesCandidates(msg.getSender(), geolocation));
				} else {
					reply.setContentObject(new ArrayList<>());
				}
				myAgent.send(reply);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			block();
		}
	}
	
	private SupernodeSearchResponse selectSupernodesCandidates(AID nodeAID, Geolocation geolocation) {
		ArrayList<ServerInfo> supernodesInfoList;
		
		List<ServerInfo> supernodes = ((CloudManagerAgent) myAgent).supernodesDatabase.searchForSupernodes(geolocation).stream().filter(aid -> !aid.supernodeAID.equals(nodeAID)).map(aid -> new ServerInfo(aid.supernodeAID.getName(), aid.ip, aid.port, aid.supernodeAID, aid.geolocation)).collect(Collectors.toList());
		
		SupernodeSearchResponse supernodeSearchResponse = new SupernodeSearchResponse();
		supernodeSearchResponse.supernodes = supernodes;
		
		if(((CloudManagerAgent) myAgent).getRatingDatabase() != null) {
			ArrayList<AID> referrals = new ArrayList<>();
			for(ServerInfo serverInfo : supernodes) {
				referrals.addAll(((CloudManagerAgent) myAgent).getRatingDatabase().recoverReferralsForServer(serverInfo.getSever()));
			}
			supernodeSearchResponse.referrals = referrals.stream().distinct().collect(Collectors.toList());
		}
		
		System.out.println("Cloud Manager found " + supernodes.size() + " supernodes");
		return supernodeSearchResponse;
	}

}
