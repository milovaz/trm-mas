package br.com.tm.cloudmanager;

import br.com.tm.report.Report;
import br.com.tm.report.ReportFactory;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class SimulationDataServiceBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;

	private static final String SIMULATION_STATS_CONVERSATION = "sim-stats-certificate";
	
	MessageTemplate mtSimulationStats = MessageTemplate.and(
			MessageTemplate.MatchConversationId(SIMULATION_STATS_CONVERSATION),
			MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_QUERY)
	);
	
	private Report summaryReport;
	
	public SimulationDataServiceBehaviour() {
		summaryReport = ReportFactory.getReport("summary_report");
		summaryReport.add(Report.REPORT_HEADER);
	}
	
	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mtSimulationStats);
		if(msg != null) {
			String simulationStats = msg.getContent();
			summaryReport.add(simulationStats);
			ACLMessage reply = msg.createReply();
			reply.setContent("ACK");
			reply.setPerformative(ACLMessage.INFORM);
			myAgent.send(reply);
		} else {
			block();
		}
	}

}
