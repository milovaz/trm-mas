package br.com.tm.cloudmanager;

import br.com.tm.util.Geolocation;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class LocalCoordinatorRegistrationServiceBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;

	private static final String LOCAL_COORDINATOR_REGISTRATION_CONVERSATION_ID = "local-coordinator-registration-conversation-id";
	
	MessageTemplate mt = MessageTemplate.and(
			MessageTemplate.MatchConversationId(LOCAL_COORDINATOR_REGISTRATION_CONVERSATION_ID),
			MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE)
	);
	
	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mt);
		if(msg != null) {
			ACLMessage reply = msg.createReply();
			reply.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
			switch (msg.getPerformative()) {
				case ACLMessage.SUBSCRIBE:
					if(getAgent().isAlive()) {
						reply.setPerformative(ACLMessage.AGREE);
						String params = msg.getContent();
						String [] paramsParts = params.split(";");
						int capacity = Integer.valueOf(paramsParts[0]);
						double lat = Double.valueOf(paramsParts[1]);
						double lon = Double.valueOf(paramsParts[2]);
						
						getAgent().localCoordinatorDatabase.addLocalCoordinator(msg.getSender(), new Geolocation(lat, lon), capacity);
						
						System.out.println("Agreeing to " + msg.getSender().getLocalName() + " registry");
					} else {
						reply.setPerformative(ACLMessage.REFUSE);
						System.out.println("Refusing local coordinator registry");
					}
				break;
	
				case ACLMessage.CANCEL:
					getAgent().localCoordinatorDatabase.removeLocalCoordinator(msg.getSender());
				break;
			}
			
			myAgent.send(reply);
		} else {
			block();
		}
	}
	
	@Override
	public CloudManagerAgent getAgent() {
		return(CloudManagerAgent) myAgent;
	}

}
