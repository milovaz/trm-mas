package br.com.tm.cloudmanager;

import br.com.tm.util.Configuration;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

/**
 * This behaviour is responsible for controlling the fog infrastructure. It employes a Objective Oriented approach to achievi its goals. 
 * Once a state of the underlying fog infraestructure (which is treated as the environment state of the CloudManagerAgent) is detected it identifies a goal to be persued
 * and defines a plan formed by a sequence of steps. Each plan is implemented as a Behaviour.
 * 
 * Periodic responsabilities:
 * 
 * - Calculate general trust.
 * - Verify Local Coordinators (if the case)
 * - Insert supernodes on watch-list
 * 
 * @author milovaz
 *
 */
public class FogControlBehaviour extends TickerBehaviour {

	private static final long serialVersionUID = 1L;
	
	public FogControlBehaviour(Agent a, long period) {
		super(a, period);
	}
	
	@Override
	protected void onTick() {
		controlGeneralTrust();
		checkSupernodesServiceQuality();
	}
	
	protected void controlGeneralTrust() {
		
	}
	
	protected void checkSupernodesServiceQuality() {
	}
	
	protected void checkSupernodesWatchList() {
		
	}
	
	protected void checkLocalCoordinatorServiceQuality() {
		
	}
	
	public CloudManagerAgent getAgent() {
		return (CloudManagerAgent) myAgent;
	}
	
}
