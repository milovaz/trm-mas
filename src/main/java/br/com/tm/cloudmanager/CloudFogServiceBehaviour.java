package br.com.tm.cloudmanager;

import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.messaging.MessageStorage;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class CloudFogServiceBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;

	private static final String ADD_TO_SUPPORTED_LIST = "add-to-supported-list";
	private static final String NODE_DISCONNECT_FOG_SERVER_CONVERSATION_ID = "node-disconnect-fog-server-conversation-id";
	
	MessageTemplate mtSubscribeToCloudService = MessageTemplate.and(
			MessageTemplate.or(
				MessageTemplate.MatchConversationId(ADD_TO_SUPPORTED_LIST),
				MessageTemplate.MatchConversationId(NODE_DISCONNECT_FOG_SERVER_CONVERSATION_ID)
			),
			MessageTemplate.and(
					MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE),
					MessageTemplate.or(
							MessageTemplate.MatchPerformative(ACLMessage.SUBSCRIBE),
							MessageTemplate.MatchPerformative(ACLMessage.CANCEL)
					)
			)
			
	);	
	
	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mtSubscribeToCloudService);
		if(msg != null) {
			System.out.println(myAgent.getLocalName() +  " received message asking for subscription");
			ACLMessage reply = msg.createReply();
			switch (msg.getPerformative()) {
				case ACLMessage.SUBSCRIBE:
					if(((CloudManagerAgent) myAgent).isAlive()) {
						reply.setPerformative(ACLMessage.AGREE);
						addToSupportedNodeList(msg.getSender());
					} else {
						reply.setPerformative(ACLMessage.REFUSE);
					}
					myAgent.send(reply);
				break;
	
				case ACLMessage.CANCEL:
					this.removeFromSupportedNodeList(msg.getSender());
				break;
			}
		} else {
			block();
		}
	}
	
	private void removeFromSupportedNodeList(AID nodeAID) {
		if(((CloudManagerAgent) myAgent).supportedNodes.size() > 0) {
			((CloudManagerAgent) myAgent).supportedNodes.remove(nodeAID);
			//((CloudManagerAgent) myAgent).udpServer.setClientsCount(((CloudManagerAgent) myAgent).udpServer.getClientsCount() - 1);
		}
	}
	
	synchronized private void addToSupportedNodeList(AID nodeAID) {
		CloudManagerAgent agent = (CloudManagerAgent) myAgent;
		if(agent.supportedNodes.isEmpty()) {
			agent.supportedNodes.add(nodeAID);
			//agent.udpServer.setClientsCount(agent.udpServer.getClientsCount() + 1);
		} else {
			if(agent.supportedNodes.stream().filter(node -> node.getName().equalsIgnoreCase(nodeAID.getName())).count() == 0) {
				agent.supportedNodes.add(nodeAID);
				//agent.udpServer.setClientsCount(agent.udpServer.getClientsCount() + 1);
			}
		}
	}

}
