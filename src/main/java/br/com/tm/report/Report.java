package br.com.tm.report;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Clock;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.stream.MessageStats;

public class Report {
	
	public static final String REPORT_HEADER = "DATE;NODE;SEVER;QTD MSGS;QTD lOSSES;RATIO;IS SATISFIED";
	
	private String name;
	private FileWriter fw;
	private BufferedWriter bw;
	
	public Report(String fileName) throws IOException {
		File file = new File(fileName);
		fw = new FileWriter(file.getAbsolutePath(), false);
		bw = new BufferedWriter(fw);
		name = fileName;
	}
	
	public void add(String data) {
		try {
			bw.write(data + "\n");
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String addRatingData(Rating rating, MessageStats ms) {
		//Iteration;Datetime;Node name;Server name;Loss pct;satisfied?
		String logLine = rating.getIteration() + ";" +
						 Clock.systemUTC().instant().toEpochMilli() + ";" +
						 rating.getNodeName() + ";" +
						 rating.getServerName() + ";" +
						 ms.countMessages + ";" +
						 ms.countLosses + ";" +
						 (double)((double)ms.countLosses / (double)ms.countMessages) + ";" +
						 rating.isSatisfied();
		
		add(logLine);
		
		return logLine;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public FileWriter getFw() {
		return fw;
	}

	public void setFw(FileWriter fw) {
		this.fw = fw;
	}
	
	public void close() {
		try {
			if (bw != null)
				bw.close();

			if(fw != null) {
				fw.close();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	
	}
	
	@Override
	protected void finalize() throws Throwable {
		close();		
		super.finalize();
	}
	
}
