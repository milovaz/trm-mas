package br.com.tm.report;

import java.io.IOException;

import jade.core.Agent;

public class ReportFactory {

	public static Report getReport(String name) {
		return ReportFactory.createReport(name);
	}
	
	public static Report getReport(Agent agent) {
		return ReportFactory.createReport(agent.getLocalName());
	}
	
	public static Report getSimulationReport(Agent agent) {
		String reportFilePath = "sim_report_" + agent.getLocalName();
		return ReportFactory.createReport(reportFilePath);
	}
	
	public static Report getTraceReport(Agent agent) {
		String reportFilePath = "trace_" + agent.getLocalName();
		return ReportFactory.createReport(reportFilePath);
	}
	
	private static Report createReport(String name) {
		String reportFilePath = name + ".txt";
		Report report;
		try {
			report = new Report(reportFilePath);
			
			return report;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}
