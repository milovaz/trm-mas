package br.com.tm.util;

import java.util.concurrent.ThreadLocalRandom;

public class SimulatorGen {
	
	/**
	 * Parameters for simulation and tests 
	 */
	protected static final int SECOND = 1000; 
	protected static final int MINUTE = SECOND; //Adjusting - means that 1 MINUTE == 1 SECOND
	protected static final int HOUR = 60 * MINUTE;
	protected static final int DAY = 24 * HOUR; 
	/*
	 * *************************************
	 */

	static double [][] GAME_DURATION_DISTRIBUTION = { 
		{0.0 , 2.0, 0.3},
		{2.0 , 5.0, 0.5},
		{5.0 , 24.0, 0.2}
	};
	
	static double [][] GAME_START_TIME = {
		{0, 18, 0.3},
		{19, 23, 0.7}
	};
	
	public static long[][] generateNodesStartEndTime(int numNodes) {
		int [] gameDurationDistribution = new int [GAME_DURATION_DISTRIBUTION.length];
		int [] gameStartDistribution = new int [GAME_START_TIME.length];
		long [][] distribution = new long[numNodes][3];
		
		for(int i = 0; i < GAME_DURATION_DISTRIBUTION.length; i++) {
			gameDurationDistribution[i] = (int)(GAME_DURATION_DISTRIBUTION[i][2] * numNodes);
		}
		
		for(int i = 0; i < GAME_START_TIME.length; i++) {
			gameStartDistribution[i] = (int)(GAME_START_TIME[i][2] * numNodes);
		}
		
		for(int i = 0; i < numNodes; i++) {
			int gameStartIndex = ThreadLocalRandom.current().nextInt(gameStartDistribution.length);
			while(gameStartDistribution[gameStartIndex] == 0) {
				gameStartIndex = (++gameStartIndex) % gameStartDistribution.length;
			}
			
			int gameDurationIndex = ThreadLocalRandom.current().nextInt(gameDurationDistribution.length);
			while(gameDurationDistribution[gameDurationIndex] == 0) {
				gameDurationIndex = (++gameDurationIndex) % gameDurationDistribution.length;
			}
			
			distribution[i][0] = (long)GAME_START_TIME[gameStartIndex][0];
			distribution[i][1] = (long)GAME_START_TIME[gameStartIndex][1];
			distribution[i][2] = (ThreadLocalRandom.current().nextLong((long)GAME_DURATION_DISTRIBUTION[gameDurationIndex][0], (long)GAME_DURATION_DISTRIBUTION[gameDurationIndex][1]) + 1) * Configuration.HOUR;
			
			gameStartDistribution[gameStartIndex]--;
			gameDurationDistribution[gameDurationIndex]--;
		}
		
		return distribution;
	}
	
	public static void main(String[] args) {
		long [][] distribution = SimulatorGen.generateNodesStartEndTime(100);
		for(int i = 0; i < distribution.length; i++) {
			for(int j = 0; j < distribution[i].length; j++) {
				System.out.print(distribution[i][j] + " ");
			}
			System.out.println("");
		}
	}
}
