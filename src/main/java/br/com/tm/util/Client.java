package br.com.tm.util;

import java.util.concurrent.BlockingQueue;

public class Client implements Runnable {

	private final BlockingQueue<String> queue;
	private int count = 1;
	
	public Client(BlockingQueue<String> q) {
		this.queue = q;
	}
	
	@Override
	public void run() {
		try {
			while(true) {
				Thread.sleep(5000);
				this.queue.put(produce());
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public String produce() {
		return "CONTADOR " + count++;
	}
	
}
