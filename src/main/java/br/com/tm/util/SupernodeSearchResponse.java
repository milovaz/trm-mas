package br.com.tm.util;

import java.io.Serializable;
import java.util.List;

import jade.core.AID;

public class SupernodeSearchResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public List<ServerInfo> supernodes;
	public List<AID> referrals;
	
	public SupernodeSearchResponse(List<ServerInfo> supernodes, List<AID> referrals) {
		super();
		this.supernodes = supernodes;
		this.referrals = referrals;
	}
	
	public SupernodeSearchResponse() {
	
	}
	
}
