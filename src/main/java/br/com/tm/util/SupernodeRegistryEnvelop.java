package br.com.tm.util;

import jade.core.AID;
import jade.util.leap.Serializable;

public class SupernodeRegistryEnvelop implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public double pctToFail;
	public int fogServiceModelValue;
	public AID localCoordinator;
	
	public SupernodeRegistryEnvelop(double pctToFail, int fogServiceModelValue, AID localCoordinator) {
		super();
		this.pctToFail = pctToFail;
		this.fogServiceModelValue = fogServiceModelValue;
		this.localCoordinator = localCoordinator;
	}
	
	@Override
	public String toString() {
		return "{pctToFail: " + this.pctToFail + 
				", serviceModel: " + FogServiceModel.parse(this.fogServiceModelValue) + 
				", localCoordinator: " + (this.localCoordinator != null ? this.localCoordinator.getLocalName() : "null") + 
				"}";
				
	}
}
