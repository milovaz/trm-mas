package br.com.tm.util;

import java.io.Serializable;

import jade.core.AID;

public class ServerInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String sever;
	private String serverIp;
	private int port;
	private AID aid;
	private Geolocation geolocation;
	
	public ServerInfo(String sever, String serverIp, int port, AID aid, Geolocation geolocation) {
		this.sever = sever;
		this.serverIp = serverIp;
		this.port = port;
		this.aid = aid;
		this.geolocation = geolocation;
	}
	
	public ServerInfo(String sever, String serverIp, int port, AID aid) {
		this.sever = sever;
		this.serverIp = serverIp;
		this.port = port;
		this.aid = aid;
	}
	
	public String getSever() {
		return sever;
	}
	public void setSever(String sever) {
		this.sever = sever;
	}
	public String getServerIp() {
		return serverIp;
	}
	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public AID getAid() {
		return aid;
	}
	public void setAid(AID aid) {
		this.aid = aid;
	}
	public Geolocation getGeolocation() {
		return geolocation;
	}
	public void setGeolocation(Geolocation geolocation) {
		this.geolocation = geolocation;
	}
	
}
