package br.com.tm.util;

public class LCStats {

	private int countDownTimes;
	private int countInterations;
	private int countQoSFailure;
	private int countSupernodeConsumption;
	
	public int getCountDownTimes() {
		return countDownTimes;
	}
	public void setCountDownTimes(int countDownTimes) {
		this.countDownTimes = countDownTimes;
	}
	public int getCountInterations() {
		return countInterations;
	}
	public void setCountInterations(int countInterations) {
		this.countInterations = countInterations;
	}
	public int getCountQoSFailure() {
		return countQoSFailure;
	}
	public void setCountQoSFailure(int countQoSFailure) {
		this.countQoSFailure = countQoSFailure;
	}
	public int getCountSupernodeConsumption() {
		return countSupernodeConsumption;
	}
	public void setCountSupernodeConsumption(int countSupernodeConsumption) {
		this.countSupernodeConsumption = countSupernodeConsumption;
	}
	
}
