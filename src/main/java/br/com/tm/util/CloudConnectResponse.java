package br.com.tm.util;

import java.io.Serializable;

import jade.core.AID;

public class CloudConnectResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public int fogServiceModelValue;
	public AID localCoordinator;
	
	public CloudConnectResponse(int fogServiceModelValue, AID localCoordinator) {
		super();
		this.fogServiceModelValue = fogServiceModelValue;
		this.localCoordinator = localCoordinator;
	}
	
}
