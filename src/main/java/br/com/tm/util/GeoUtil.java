package br.com.tm.util;

public class GeoUtil {

	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  Geolocation functions                                         :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	public double distance(Geolocation geolocationA, Geolocation geolocationB) {
		return distance(geolocationA.getLatitude(), geolocationA.getLongitude(), geolocationB.getLatitude(), geolocationB.getLongitude(), 'K');
	}
	
	public double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == 'K') {
			dist = dist * 1.609344;
		} else if (unit == 'N') {
			dist = dist * 0.8684;
		}
		return dist;
	}
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts decimal degrees to radians             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts radians to decimal degrees             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}
	
	public int kmToLatencyRatio(double distInKm) { 
		return (int)Math.round(1000*((distInKm * 1000) / 1.85e8));  
	}
	
	public double latencyToKmRatio(int latencyInMs) {
		return (1.85e8 * latencyInMs) / (1000000);
	}
	
	public static void main(String[] args) {
		GeoUtil u = new GeoUtil();
		System.out.println(u.distance(45.628, -110.9013, 47.6854, -122.2872, 'K'));
	}
}
