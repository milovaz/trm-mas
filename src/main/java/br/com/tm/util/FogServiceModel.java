package br.com.tm.util;

public enum FogServiceModel {
	OPEN(0, "just the cloud server is fully controlled by the service provider - there's no local coordinator"),
	PARTIALLY_OPEN(1, "the cloud server and local coordinators are fully controlled by the service provider"),
	FIXED(2, "all elements, excluding the nodes, are controllerd by the service provider");
	
	public int value;
	public String description;
	
	private FogServiceModel(int value, String description) {
		this.value = value;
		this.description = description;
	}
	
	public static FogServiceModel parse(int value) {
		for(FogServiceModel fs : FogServiceModel.values()) {
			if(fs.value == value) {
				return fs;
			}
		}
		
		return null;
	}

}
